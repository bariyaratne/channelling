VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Session"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varFacilitySecession_ID As Long
    Private varHospitalFacility_ID As Long
    Private varFacilityStaff_ID As Long
    Private varStaff_ID As Long
    Private varSecessionName As String
    Private varStartingTime As Date
    Private varUsualDuration As Long
    Private varMaximum As Long
    Private varLocalDoctorFee As Double
    Private varLocalTax As Double
    Private varForeignTax As Double
    Private varAgentTax As Double
    Private varForeignDoctorFee As Double
    Private varAgentDoctorFee As Double
    Private varLocalHospitalFee As Double
    Private varForeignHospitalFee As Double
    Private varAgentHospitalFee As Double
    Private varCanByPassOrder As Boolean
    Private varSecessionWeekday As Long
    Private varChangeDate As Date
    Private varFullDayLeave As Boolean
    Private varCalculateAppointment As Boolean
    Private varComments As String
    Private varAlteredDate As Date
    Private varRoomNo As String
    Private varStartingNo As Long
    Private varIncreaseNo As Long
    Private varIncreaseMin As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblFacilitySecession"
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If varFacilitySecession_ID = 0 Then .AddNew
        !HospitalFacility_ID = varHospitalFacility_ID
        !FacilityStaff_ID = varFacilityStaff_ID
        !Staff_ID = varStaff_ID
        !SecessionName = varSecessionName
        !StartingTime = varStartingTime
        !UsualDuration = varUsualDuration
        !Maximum = varMaximum
        !LocalDoctorFee = varLocalDoctorFee
        !LocalTax = varLocalTax
        !ForeignTax = varForeignTax
        !AgentTax = varAgentTax
        !ForeignDoctorFee = varForeignDoctorFee
        !AgentDoctorFee = varAgentDoctorFee
        !LocalHospitalFee = varLocalHospitalFee
        !ForeignHospitalFee = varForeignHospitalFee
        !AgentHospitalFee = varAgentHospitalFee
        !CanByPassOrder = varCanByPassOrder
        !SecessionWeekday = varSecessionWeekday
        !ChangeDate = varChangeDate
        !FullDayLeave = varFullDayLeave
        !CalculateAppointment = varCalculateAppointment
        !Comments = varComments
        !AlteredDate = varAlteredDate
        !RoomNo = varRoomNo
        !StartingNo = varStartingNo
        !IncreaseNo = varIncreaseNo
        !IncreaseMin = varIncreaseMin
        .Update
        varFacilitySecession_ID = !FacilitySecession_ID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblFacilitySecession WHERE FacilitySecession_ID = " & varFacilitySecession_ID
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If Not IsNull(!FacilitySecession_ID) Then
           varFacilitySecession_ID = !FacilitySecession_ID
        End If
        If Not IsNull(!HospitalFacility_ID) Then
           varHospitalFacility_ID = !HospitalFacility_ID
        End If
        If Not IsNull(!FacilityStaff_ID) Then
           varFacilityStaff_ID = !FacilityStaff_ID
        End If
        If Not IsNull(!Staff_ID) Then
           varStaff_ID = !Staff_ID
        End If
        If Not IsNull(!SecessionName) Then
           varSecessionName = Trim(!SecessionName)
        End If
        If Not IsNull(!StartingTime) Then
           varStartingTime = !StartingTime
        End If
        If Not IsNull(!UsualDuration) Then
           varUsualDuration = !UsualDuration
        End If
        If Not IsNull(!Maximum) Then
           varMaximum = !Maximum
        End If
        If Not IsNull(!LocalDoctorFee) Then
           varLocalDoctorFee = !LocalDoctorFee
        End If
        If Not IsNull(!LocalTax) Then
           varLocalTax = !LocalTax
        End If
        If Not IsNull(!ForeignTax) Then
           varForeignTax = !ForeignTax
        End If
        If Not IsNull(!AgentTax) Then
           varAgentTax = !AgentTax
        End If
        If Not IsNull(!ForeignDoctorFee) Then
           varForeignDoctorFee = !ForeignDoctorFee
        End If
        If Not IsNull(!AgentDoctorFee) Then
           varAgentDoctorFee = !AgentDoctorFee
        End If
        If Not IsNull(!LocalHospitalFee) Then
           varLocalHospitalFee = !LocalHospitalFee
        End If
        If Not IsNull(!ForeignHospitalFee) Then
           varForeignHospitalFee = !ForeignHospitalFee
        End If
        If Not IsNull(!AgentHospitalFee) Then
           varAgentHospitalFee = !AgentHospitalFee
        End If
        If Not IsNull(!CanByPassOrder) Then
           varCanByPassOrder = !CanByPassOrder
        End If
        If Not IsNull(!SecessionWeekday) Then
           varSecessionWeekday = !SecessionWeekday
        End If
        If Not IsNull(!ChangeDate) Then
           varChangeDate = !ChangeDate
        End If
        If Not IsNull(!FullDayLeave) Then
           varFullDayLeave = !FullDayLeave
        End If
        If Not IsNull(!CalculateAppointment) Then
           varCalculateAppointment = !CalculateAppointment
        End If
        If Not IsNull(!Comments) Then
           varComments = !Comments
        End If
        If Not IsNull(!AlteredDate) Then
           varAlteredDate = !AlteredDate
        End If
        If Not IsNull(!RoomNo) Then
           varRoomNo = !RoomNo
        End If
        If Not IsNull(!StartingNo) Then
           varStartingNo = !StartingNo
        End If
        If Not IsNull(!IncreaseNo) Then
           varIncreaseNo = !IncreaseNo
        End If
        If Not IsNull(!IncreaseMin) Then
           varIncreaseMin = !IncreaseMin
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varFacilitySecession_ID = 0
    varHospitalFacility_ID = 0
    varFacilityStaff_ID = 0
    varStaff_ID = 0
    varSecessionName = Empty
    varStartingTime = Empty
    varUsualDuration = 0
    varMaximum = 0
    varLocalDoctorFee = 0
    varLocalTax = 0
    varForeignTax = 0
    varAgentTax = 0
    varForeignDoctorFee = 0
    varAgentDoctorFee = 0
    varLocalHospitalFee = 0
    varForeignHospitalFee = 0
    varAgentHospitalFee = 0
    varCanByPassOrder = False
    varSecessionWeekday = 0
    varChangeDate = Empty
    varFullDayLeave = False
    varCalculateAppointment = False
    varComments = Empty
    varAlteredDate = Empty
    varRoomNo = Empty
    varStartingNo = 0
    varIncreaseNo = 0
    varIncreaseMin = 0
End Sub

Public Property Let FacilitySecession_ID(ByVal vFacilitySecession_ID As Long)
    Call clearData
    varFacilitySecession_ID = vFacilitySecession_ID
    Call loadData
End Property

Public Property Get FacilitySecession_ID() As Long
    FacilitySecession_ID = varFacilitySecession_ID
End Property

Public Property Let HospitalFacility_ID(ByVal vHospitalFacility_ID As Long)
    varHospitalFacility_ID = vHospitalFacility_ID
End Property

Public Property Get HospitalFacility_ID() As Long
    HospitalFacility_ID = varHospitalFacility_ID
End Property

Public Property Let FacilityStaff_ID(ByVal vFacilityStaff_ID As Long)
    varFacilityStaff_ID = vFacilityStaff_ID
End Property

Public Property Get FacilityStaff_ID() As Long
    FacilityStaff_ID = varFacilityStaff_ID
End Property

Public Property Let Staff_ID(ByVal vStaff_ID As Long)
    varStaff_ID = vStaff_ID
End Property

Public Property Get Staff_ID() As Long
    Staff_ID = varStaff_ID
End Property

Public Property Let SecessionName(ByVal vSecessionName As String)
    varSecessionName = vSecessionName
End Property

Public Property Get SecessionName() As String
    SecessionName = varSecessionName
End Property

Public Property Let StartingTime(ByVal vStartingTime As Date)
    varStartingTime = vStartingTime
End Property

Public Property Get StartingTime() As Date
    StartingTime = varStartingTime
End Property

Public Property Let UsualDuration(ByVal vUsualDuration As Long)
    varUsualDuration = vUsualDuration
End Property

Public Property Get UsualDuration() As Long
    UsualDuration = varUsualDuration
End Property

Public Property Let Maximum(ByVal vMaximum As Long)
    varMaximum = vMaximum
End Property

Public Property Get Maximum() As Long
    Maximum = varMaximum
End Property

Public Property Let LocalDoctorFee(ByVal vLocalDoctorFee As Double)
    varLocalDoctorFee = vLocalDoctorFee
End Property

Public Property Get LocalDoctorFee() As Double
    LocalDoctorFee = varLocalDoctorFee
End Property

Public Property Let LocalTax(ByVal vLocalTax As Double)
    varLocalTax = vLocalTax
End Property

Public Property Get LocalTax() As Double
    LocalTax = varLocalTax
End Property

Public Property Let ForeignTax(ByVal vForeignTax As Double)
    varForeignTax = vForeignTax
End Property

Public Property Get ForeignTax() As Double
    ForeignTax = varForeignTax
End Property

Public Property Let AgentTax(ByVal vAgentTax As Double)
    varAgentTax = vAgentTax
End Property

Public Property Get AgentTax() As Double
    AgentTax = varAgentTax
End Property

Public Property Let ForeignDoctorFee(ByVal vForeignDoctorFee As Double)
    varForeignDoctorFee = vForeignDoctorFee
End Property

Public Property Get ForeignDoctorFee() As Double
    ForeignDoctorFee = varForeignDoctorFee
End Property

Public Property Let AgentDoctorFee(ByVal vAgentDoctorFee As Double)
    varAgentDoctorFee = vAgentDoctorFee
End Property

Public Property Get AgentDoctorFee() As Double
    AgentDoctorFee = varAgentDoctorFee
End Property

Public Property Let LocalHospitalFee(ByVal vLocalHospitalFee As Double)
    varLocalHospitalFee = vLocalHospitalFee
End Property

Public Property Get LocalHospitalFee() As Double
    LocalHospitalFee = varLocalHospitalFee
End Property

Public Property Let ForeignHospitalFee(ByVal vForeignHospitalFee As Double)
    varForeignHospitalFee = vForeignHospitalFee
End Property

Public Property Get ForeignHospitalFee() As Double
    ForeignHospitalFee = varForeignHospitalFee
End Property

Public Property Let AgentHospitalFee(ByVal vAgentHospitalFee As Double)
    varAgentHospitalFee = vAgentHospitalFee
End Property

Public Property Get AgentHospitalFee() As Double
    AgentHospitalFee = varAgentHospitalFee
End Property

Public Property Let CanByPassOrder(ByVal vCanByPassOrder As Boolean)
    varCanByPassOrder = vCanByPassOrder
End Property

Public Property Get CanByPassOrder() As Boolean
    CanByPassOrder = varCanByPassOrder
End Property

Public Property Let SecessionWeekday(ByVal vSecessionWeekday As Long)
    varSecessionWeekday = vSecessionWeekday
End Property

Public Property Get SecessionWeekday() As Long
    SecessionWeekday = varSecessionWeekday
End Property

Public Property Let ChangeDate(ByVal vChangeDate As Date)
    varChangeDate = vChangeDate
End Property

Public Property Get ChangeDate() As Date
    ChangeDate = varChangeDate
End Property

Public Property Let FullDayLeave(ByVal vFullDayLeave As Boolean)
    varFullDayLeave = vFullDayLeave
End Property

Public Property Get FullDayLeave() As Boolean
    FullDayLeave = varFullDayLeave
End Property

Public Property Let CalculateAppointment(ByVal vCalculateAppointment As Boolean)
    varCalculateAppointment = vCalculateAppointment
End Property

Public Property Get CalculateAppointment() As Boolean
    CalculateAppointment = varCalculateAppointment
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let AlteredDate(ByVal vAlteredDate As Date)
    varAlteredDate = vAlteredDate
End Property

Public Property Get AlteredDate() As Date
    AlteredDate = varAlteredDate
End Property

Public Property Let RoomNo(ByVal vRoomNo As String)
    varRoomNo = vRoomNo
End Property

Public Property Get RoomNo() As String
    RoomNo = varRoomNo
End Property

Public Property Let StartingNo(ByVal vStartingNo As Long)
    varStartingNo = vStartingNo
End Property

Public Property Get StartingNo() As Long
    StartingNo = varStartingNo
End Property

Public Property Let IncreaseNo(ByVal vIncreaseNo As Long)
    varIncreaseNo = vIncreaseNo
End Property

Public Property Get IncreaseNo() As Long
    IncreaseNo = varIncreaseNo
End Property

Public Property Let IncreaseMin(ByVal vIncreaseMin As Long)
    varIncreaseMin = vIncreaseMin
End Property

Public Property Get IncreaseMin() As Long
    IncreaseMin = varIncreaseMin
End Property


