VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsReportPrinting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim RemainingNotes As String
    Dim CurrentNotes As String
    Dim PresentCurrentNotes As String
    Dim RemainingCurrentNotes As String
    
    Dim RemainingNotesLength As Long
    Dim CurrentNotesLength As Long
    Dim PresentCurrentNotesLength As Long
    Dim RemainingCurrentNotesLength As Long
    
    Dim TextTopY As Long
    Dim TextBotY As Long
    Dim TextLX As Long
    Dim TextRX As Long
    
    Dim PaperTopY As Long
    Dim PaperBotY As Long
    Dim PaperLeftX As Long
    Dim PaperRightX As Long
    
    Dim TextXMargin As Single
    Dim TextYMargin As Single
    Dim InbetweenX As Single
    Dim InbetweenY As Single

Private Sub AddNotes(SendingText As String)
    With Printer
    RemainingNotes = SendingText
    While InStr(RemainingNotes, Chr(13) & Chr(10)) > 0
        CurrentNotesLength = InStr(RemainingNotes, Chr(13) & Chr(10))
        CurrentNotes = Left(RemainingNotes, CurrentNotesLength - 1)
        PrintCurrentNotes
        RemainingNotesLength = Len(RemainingNotes) - (CurrentNotesLength + 1)
        RemainingNotes = Right(RemainingNotes, RemainingNotesLength)
    Wend
    CurrentNotes = Right(RemainingNotes, (Len(RemainingNotes) - 0))
    PrintCurrentNotes
    .CurrentY = .CurrentY + InbetweenY
    TextBotY = .CurrentY
    End With
End Sub

Private Sub PrintCurrentNotes()
    With Printer
        If .TextWidth(CurrentNotes) > TextRX - (TextLX) Then
            BreakCurrentNotes
        Else
            .CurrentX = TextLX
            PrintingWithSuperscript (CurrentNotes)
        End If
    End With
End Sub

Private Sub BreakCurrentNotes()
    If InStr(CurrentNotes, " ") < 1 Then
        BreakNoSpaces
    Else
        BreakSpaces
    End If
End Sub

Private Sub BreakSpaces()
    With Printer
        Dim SearchPosition
        RemainingCurrentNotes = CurrentNotes
        SearchPosition = Len(RemainingCurrentNotes)
        While InStr(RemainingCurrentNotes, " ") > 0
            RemainingCurrentNotesLength = Len(RemainingCurrentNotes)
            If SearchPosition = 0 Then SearchPosition = 1
            PresentCurrentNotesLength = InStrRev(RemainingCurrentNotes, " ", SearchPosition)
            PresentCurrentNotes = Left(RemainingCurrentNotes, PresentCurrentNotesLength)
            If Printer.TextWidth(PresentCurrentNotes) < TextRX - (TextLX) Then
                .CurrentX = TextLX
                PrintingWithSuperscript (PresentCurrentNotes)
                RemainingCurrentNotes = Right(RemainingCurrentNotes, RemainingCurrentNotesLength - PresentCurrentNotesLength)
                SearchPosition = Len(RemainingCurrentNotes)
            Else
                SearchPosition = SearchPosition - 1
            End If
        Wend
        If Printer.TextWidth(RemainingCurrentNotes) < TextRX - (TextLX) Then
            .CurrentX = TextLX
            PrintingWithSuperscript (RemainingCurrentNotes)
        Else
            BreakNoSpaces
        End If
    End With
End Sub

Private Sub BreakNoSpaces()
    Dim A
    Dim TextLength
    Dim temText
    
    With Printer
        TextLength = 0
        .CurrentX = TextLX
        For A = 1 To Len(CurrentNotes)
            temText = Mid(CurrentNotes, A, 1)
            TextLength = TextLength + Printer.TextWidth(temText)
            If TextLength >= TextRX - (TextLX) Then
                Printer.Print temText
                TextLength = 0
                .CurrentX = TextLX
            Else
                Printer.Print temText;
            End If
        Next
    End With
End Sub


Private Sub PrintingWithSuperscript(ByVal SuppliedForSuperscript As String)
    Dim SuperscriptPresent As Boolean
    Dim SuperscriptLocation As Long
    Dim BeforeSuperscriptText As String
    Dim AfterSuperscriptText As String
    Dim SuperScriptText As String
    Dim SpaceLocation As Long
    Dim SendingX As Long
    Dim SendingY As Long
    Dim SuperscriptElevation As Double
    
    SendingX = Printer.CurrentX
    SendingY = Printer.CurrentY
    
    SuppliedForSuperscript = SuppliedForSuperscript
    
    SuperscriptLocation = InStr(1, SuppliedForSuperscript, "^")
    
    SuperscriptElevation = 0.2
    
    If SuperscriptLocation = 0 Then
        Printer.CurrentX = SendingX
        Printer.CurrentY = SendingY
        Printer.Print SuppliedForSuperscript
    Else
        If SuperscriptLocation > 0 Then BeforeSuperscriptText = Left(SuppliedForSuperscript, SuperscriptLocation - 1)
        AfterSuperscriptText = Right(SuppliedForSuperscript, (Len(SuppliedForSuperscript) - SuperscriptLocation))
        AfterSuperscriptText = Trim(AfterSuperscriptText)
        If InStr(1, AfterSuperscriptText, " ") = 0 Then
            Printer.CurrentX = SendingX
            Printer.CurrentY = SendingY
            Printer.Print BeforeSuperscriptText
            Printer.CurrentX = SendingX + Printer.TextWidth(BeforeSuperscriptText)
            Printer.CurrentY = SendingY - (Printer.TextHeight(BeforeSuperscriptText) * SuperscriptElevation)
            Printer.FontSize = Printer.FontSize - 2
            Printer.Print AfterSuperscriptText
            Printer.FontSize = Printer.FontSize + 2
        Else
            SpaceLocation = InStr(1, AfterSuperscriptText, " ")
            SuperScriptText = Left(AfterSuperscriptText, SpaceLocation)
            AfterSuperscriptText = Right(AfterSuperscriptText, (Len(AfterSuperscriptText) - SpaceLocation))
            
            Printer.CurrentX = SendingX
            Printer.CurrentY = SendingY
            
            Printer.Print BeforeSuperscriptText
            
            Printer.CurrentX = SendingX + Printer.TextWidth(BeforeSuperscriptText)
            Printer.CurrentY = SendingY - (Printer.TextHeight(BeforeSuperscriptText) * SuperscriptElevation)
            Printer.FontSize = Printer.FontSize - 2
            Printer.Print SuperScriptText
            Printer.FontSize = Printer.FontSize + 2
            Printer.CurrentY = SendingY
            Printer.CurrentX = SendingX + Printer.TextWidth(BeforeSuperscriptText & SuperScriptText)
            Printer.Print AfterSuperscriptText
            
        End If
    End If
    Printer.CurrentY = SendingY
    Printer.Print


End Sub




