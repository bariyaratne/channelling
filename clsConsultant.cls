VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsConsultant"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Speciality"
'local variable(s) to hold property value(s)
Private mvarConsultantID As Long 'local copy
Private mvarListedName As String 'local copy
Private mvarConsultantName As String 'local copy
Private mvarSpeciality As clsSpeciality 'local copy
Public Sub getDetails(ByVal id As Long)
End Sub

Public Property Set Speciality(ByVal vData As clsSpeciality)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Speciality = Form1
    Set mvarSpeciality = vData
End Property


Public Property Get Speciality() As clsSpeciality
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Speciality
    Set Speciality = mvarSpeciality
End Property



Public Property Let ConsultantName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ConsultantName = 5
    mvarConsultantName = vData
End Property


Public Property Get ConsultantName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ConsultantName
    ConsultantName = mvarConsultantName
End Property



Public Property Let ListedName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ListedName = 5
    mvarListedName = vData
End Property


Public Property Get ListedName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ListedName
    ListedName = mvarListedName
End Property



Public Property Let ConsultantID(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ConsultantID = 5
    mvarConsultantID = vData
End Property


Public Property Get ConsultantID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ConsultantID
    ConsultantID = mvarConsultantID
End Property



