VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDoctorAttendance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varID As Long
    Private vardoctorId As Long
    Private varonTime As Date
    Private varonUserId As Long
    Private varoffTime As Long
    Private varoffUserId As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblDoctorAttendance Where ID = " & varID
        If .State = 1 Then .Close
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !doctorId = vardoctorId
        If varonTime = 0 Then
            !onTime = Null
        Else
            !onTime = varonTime
        End If
        !onUserId = varonUserId
        !offTime = varoffTime
        !offUserId = varoffUserId
        .Update
        varID = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblDoctorAttendance WHERE ID = " & varID
        If .State = 1 Then .Close
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varID = !id
            End If
            If Not IsNull(!doctorId) Then
               vardoctorId = !doctorId
            End If
            If Not IsNull(!onTime) Then
               varonTime = !onTime
            End If
            If Not IsNull(!onUserId) Then
               varonUserId = !onUserId
            End If
            If Not IsNull(!offTime) Then
               varoffTime = !offTime
            End If
            If Not IsNull(!offUserId) Then
               varoffUserId = !offUserId
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varID = 0
    vardoctorId = 0
    varonTime = Empty
    varonUserId = 0
    varoffTime = 0
    varoffUserId = 0
End Sub

Public Property Let id(ByVal vID As Long)
    Call clearData
    varID = vID
    Call loadData
End Property

Public Property Get id() As Long
    id = varID
End Property

Public Property Let doctorId(ByVal vdoctorId As Long)
    vardoctorId = vdoctorId
End Property

Public Property Get doctorId() As Long
    doctorId = vardoctorId
End Property

Public Property Let onTime(ByVal vonTime As Date)
    varonTime = vonTime
End Property

Public Property Get onTime() As Date
    onTime = varonTime
End Property

Public Property Let onUserId(ByVal vonUserId As Long)
    varonUserId = vonUserId
End Property

Public Property Get onUserId() As Long
    onUserId = varonUserId
End Property

Public Property Let offTime(ByVal voffTime As Long)
    varoffTime = voffTime
End Property

Public Property Get offTime() As Long
    offTime = varoffTime
End Property

Public Property Let offUserId(ByVal voffUserId As Long)
    varoffUserId = voffUserId
End Property

Public Property Get offUserId() As Long
    offUserId = varoffUserId
End Property


