VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmTodaysMyAllDoctorViceBookings 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Today's My All Bookings - Doctor-vice"
   ClientHeight    =   7920
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12615
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7920
   ScaleWidth      =   12615
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   240
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Tag             =   "SS"
      Top             =   7320
      Width           =   5415
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "&Process"
      Height          =   375
      Left            =   6120
      TabIndex        =   9
      Top             =   1080
      Width           =   3735
   End
   Begin MSFlexGridLib.MSFlexGrid gridDoc 
      Height          =   5655
      Left            =   240
      TabIndex        =   8
      Top             =   1560
      Width           =   12255
      _ExtentX        =   21616
      _ExtentY        =   9975
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   495
      Left            =   11160
      TabIndex        =   5
      Top             =   7320
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnPrint 
      Height          =   495
      Left            =   9840
      TabIndex        =   4
      Top             =   7320
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Caption         =   "Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DataComboStaff 
      Bindings        =   "frmTodaysMyAllDoctorViceBookings1.frx":0000
      Height          =   360
      Left            =   1920
      TabIndex        =   2
      Top             =   120
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   1920
      TabIndex        =   3
      Top             =   600
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   156565507
      CurrentDate     =   39442
   End
   Begin MSComCtl2.DTPicker dtpApp 
      Height          =   375
      Left            =   1920
      TabIndex        =   6
      Top             =   1080
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   156565507
      CurrentDate     =   39442
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   8520
      TabIndex        =   10
      Top             =   7320
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      Caption         =   "Appointment Date"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   1080
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   "Booking Date"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   2415
   End
   Begin VB.Label Label1 
      Caption         =   "User"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   2175
   End
End
Attribute VB_Name = "frmTodaysMyAllDoctorViceBookings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim csetPrinter As New cSetDfltPrinter
    Dim temTopic As String
    Dim temSubTopic As String
    Dim temSelect As String
    Dim temFrom As String
    Dim temWhere As String
    Dim temGroupBy As String
    Dim temOrderBy As String
    Dim temSQL As String
    

Private Sub btnExcel_Click()
    btnProcess_Click
    GridToExcel gridDoc, temTopic, temSubTopic
End Sub

Private Sub btnProcess_Click()
    Dim D(6) As Integer
    Dim p(0) As Integer
    
    
    temSelect = "SELECT  tblTitle.Title+'  '+tblDoctor.DoctorName AS [Doctor Name],   COUNT (tblPatientFacility.PatientFacility_ID) AS [Total Patient Count] , SUM(tblPatientFacility.FullyPaidNull) AS [Fully Paid], SUM(tblPatientFacility.CancelledNull) AS [Cancellations], SUM(tblPatientFacility.RefundNull) AS [Refunds],  SUM(tblPatientFacility.PatientAbsentNull) AS [Absent],  SUM(tblPatientFacility.PersonalDue) AS [Total Doctor Fee]  "
    temFrom = "FROM tblTitle RIGHT JOIN (tblPatientFacility LEFT JOIN tblDoctor ON tblPatientFacility.Staff_ID = tblDoctor.Doctor_ID) ON tblTitle.Title_ID = tblDoctor.DoctorTitle_ID  "
    
    If PayToDoctor = True Then
        temWhere = "WHERE BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "# and FullyPaid = True and (Cancelled = False AND RepayUser_ID <> " & Val(DataComboStaff.BoundText) & " ) and Refund = False  and  ( ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID  =  0  )  or    ( User_ID <> " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  OR  ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  ) and (((tblPatientFacility.HospitalFacility_ID)=10))"
    Else
        temWhere = "WHERE BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "#  and FullyPaid = True and (Cancelled = False AND RepayUser_ID <> " & Val(DataComboStaff.BoundText) & " ) and Refund = False and patientabsent = false AND ( ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID  =  0  )  or    ( User_ID <> " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  OR  ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  ) and (((tblPatientFacility.HospitalFacility_ID)=10))"
    End If
    
    temGroupBy = "GROUP BY tblTitle.Title+' '+tblDoctor.DoctorName, tblTitle.Title, tblDoctor.DoctorName "
    
    temOrderBy = "ORDER BY tblTitle.Title+' '+tblDoctor.DoctorName, tblTitle.Title, tblDoctor.DoctorName "
    
    D(0) = 1: D(1) = 2: D(2) = 3: D(3) = 4: D(4) = 5: D(5) = 6
    
    temSQL = temSelect & temFrom & temWhere & temGroupBy & temOrderBy
    FillAnyGrid temSQL, gridDoc, 0, D, p

End Sub


Private Sub bttnPrint_OldClick()

'Const PreSHape = "SHAPE {"
'Const Sql = "SELECT tblPatientFacility.*, tblDoctor.DoctorName, tblTitle.Title FROM tblTitle RIGHT JOIN (tblPatientFacility LEFT JOIN tblDoctor ON tblPatientFacility.Staff_ID = tblDoctor.Doctor_ID) ON tblTitle.Title_ID = tblDoctor.DoctorTitle_ID Where "
'Const PostSHape = "(((tblPatientFacility.HospitalFacility_ID)=10))}  AS newMyAllDocterBookings COMPUTE newMyAllDocterBookings, COUNT(newMyAllDocterBookings.'PatientFacility_ID') AS TotalPatientCount, SUM(newMyAllDocterBookings.'CancelledNull') AS TotalCancellations, SUM(newMyAllDocterBookings.'RefundNull') AS TotalRefunds, SUM(newMyAllDocterBookings.'FullyPaidNull') AS TotalFullyPaid, SUM(newMyAllDocterBookings.'PatientAbsentNull') AS TotalAbsent, ANY(newMyAllDocterBookings.'Title') AS DoctorTitle, SUM(newMyAllDocterBookings.'PersonalDue') AS DoctorFee BY 'DoctorName'"
'
'csetPrinter.SetPrinterAsDefault (ReportPrinterName)
'
'    With DataEnvironment1
'
'
'        If .rsnewMyAllDocterBookings_Grouping.State = 1 Then .rsnewMyAllDocterBookings_Grouping.Close
'
'        'If .rscmmdAllDoctorPatients_Grouping.State = 1 Then .rscmmdAllDoctorPatients_Grouping.Close
'
'        If DetailedCount = False Then
'
''            If PayToDoctor = True Then
''                .Commands!newMyAllDocterBookings_Grouping.CommandText = PreSHape & Sql & "  BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "# and FullyPaid = True and (Cancelled = False AND CancelledUser_ID <> " & Val(DataComboStaff.BoundText) & " ) and Refund = False  and  ( User_ID = " & Val(DataComboStaff.BoundText) & " OR CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  and " & PostSHape
''            Else
''                .Commands!newMyAllDocterBookings_Grouping.CommandText = PreSHape & Sql & "  BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "#  and FullyPaid = True and (Cancelled = False AND CancelledUser_ID <> " & Val(DataComboStaff.BoundText) & " ) and Refund = False and patientabsent = false  and  ( User_ID = " & Val(DataComboStaff.BoundText) & " OR CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )   and " & PostSHape
''            End If
'
'            If PayToDoctor = True Then
'                .Commands!newMyAllDocterBookings_Grouping.CommandText = PreSHape & Sql & "  BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "# and FullyPaid = True and (Cancelled = False AND CancelledUser_ID <> " & Val(DataComboStaff.BoundText) & " ) and Refund = False  and  ( ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID  =  0  )  or    ( User_ID <> " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  OR  ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  ) and " & PostSHape
'            Else
'                .Commands!newMyAllDocterBookings_Grouping.CommandText = PreSHape & Sql & "  BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "#  and FullyPaid = True and (Cancelled = False AND CancelledUser_ID <> " & Val(DataComboStaff.BoundText) & " ) and Refund = False and patientabsent = false AND ( ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID  =  0  )  or    ( User_ID <> " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  OR  ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  ) and " & PostSHape
'            End If
'
'            .newMyAllDocterBookings_Grouping
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl1").Visible = False
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl2").Visible = True
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl3").Visible = False
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl4").Visible = False
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl10").Visible = False
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl5").Visible = False
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl7").Visible = False
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl8").Visible = False
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl9").Visible = False
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Caption = "Total Patients"
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function1").Visible = False
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function2").Visible = True
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function3").Visible = False
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function4").Visible = False
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function5").Visible = False
'
'
'            .Commands!newMyAllDocterBookings_Grouping.CommandText = PreSHape & Sql & "  BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "#   and  ( ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID  =  0  )  or    ( User_ID <> " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  OR  ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  ) and " & PostSHape
'
'            .newMyAllDocterBookings_Grouping
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl1").Visible = True
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl2").Visible = True
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl3").Visible = True
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl4").Visible = True
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl10").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl5").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl7").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl8").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl9").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Caption = "Fully Paid"
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function1").Visible = True
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function2").Visible = True
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function3").Visible = True
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function4").Visible = True
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function5").Visible = True
'
'
'
'        Else
'
'            .Commands!newMyAllDocterBookings_Grouping.CommandText = PreSHape & Sql & "  BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "#   and  ( ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID  =  0  )  or    ( User_ID <> " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  OR  ( User_ID = " & Val(DataComboStaff.BoundText) & " AND CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  ) and " & PostSHape
'
'            .newMyAllDocterBookings_Grouping
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl1").Visible = True
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl2").Visible = True
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl3").Visible = True
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl4").Visible = True
'            NewDataReportAllPatients.Sections("Section1").Controls.Item("lbl10").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl5").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl7").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl8").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl9").Visible = True
'            NewDataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Caption = "Fully Paid"
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function1").Visible = True
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function2").Visible = True
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function3").Visible = True
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function4").Visible = True
'            NewDataReportAllPatients.Sections("ReportFooter").Controls.Item("Function5").Visible = True
'        End If
'    End With
'    With NewDataReportAllPatients
'        If HospitalDetails = True Then
'            .Sections("ReportHeader").Controls.Item("InstitutionName").Caption = InstitutionName
'            .Sections("ReportHeader").Controls.Item("InstitutionAddress").Caption = InstitutionAddress
'            .Sections("ReportHeader").Controls.Item("lbldate").Caption = "By " & DataComboStaff.Text & " /  Booking " & Format(dtpDate.Value, "dd MMMM yyyy") & "  /  Appointment : " & Format(dtpApp.Value, "dd MMMM yyyy")
'            .Sections("ReportFooter").Controls.Item("ad1").Caption = LongAd
'        Else
'            .Sections("ReportHeader").Controls.Item("InstitutionName").Caption = Empty
'            .Sections("ReportHeader").Controls.Item("InstitutionAddress").Caption = Empty
'            .Sections("ReportHeader").Controls.Item("lbldate").Caption = Format(dtpDate.Value, "dd MMMM yyyy")
'            .Sections("ReportFooter").Controls.Item("ad1").Caption = LongAd
'        End If
'        Set .DataSource = DataEnvironment1
'        .Show
'    End With

End Sub

Private Sub bttnPrint_Click()
    On Error Resume Next

    Dim SetPrinter As New cSetDfltPrinter
    
    SetPrinter.SetPrinterAsDefault ""
    
    Dim MyPrinter As Printer
    
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = cmbPrinter.Text Then
            Set Printer = MyPrinter
        End If
    Next
    
    temTopic = "All Booking By Doctor"
    temSubTopic = "Booing Date : " & Format(dtpDate.Value, "dd MMM yyyy") & " Appointment Date " & Format(dtpApp.Value, "dd MMM yyyy") & " User " & DataComboStaff.Text
    
    SetPrinter.SetPrinterAsDefault cmbPrinter.Text
    
    Dim myPR As GridPrintSettings
    
    myPR = getDefaultGridPrint
    
    
    
    myPR.Topic = temTopic
    myPR.Subtopic = temSubTopic
    
    PrintGrid gridDoc, myPR

End Sub

Private Sub Form_Load()
    dtpDate.Value = Date
    dtpApp.Value = Date
    With DataComboStaff
        .RowMember = Empty
        .ListField = Empty
        .BoundColumn = Empty
    End With
    With DataEnvironment1.rsStaff
        If .State = 1 Then .Close
        .Source = "SELECT tblStaff.* FROM tblStaff ORDER BY StaffName"
        .Open
    End With
    With DataComboStaff
        .RowMember = "staff"
        .ListField = "StaffName"
        .BoundColumn = "Staff_ID"
    End With
    DataComboStaff.BoundText = UserID
    temTopic = "All Booking By Doctor"
    temSubTopic = "Booing Date : " & Format(dtpDate.Value, "dd MMM yyyy") & " Appointment Date " & Format(dtpApp.Value, "dd MMM yyyy") & " User " & DataComboStaff.Text
    
    Call fillPrinters
    Call GetSettings
    
    
    
End Sub


Private Sub GetSettings()
    GetCommonSettings Me
End Sub

Private Sub SaveSettings()
    SaveCommonSettings Me
End Sub

Private Sub fillPrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub
