VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSpeciality"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Speciality"
'local variable(s) to hold property value(s)
Private mvarSpecialityID As Long 'local copy
Private mvarSpeciality As String 'local copy
Public Sub getDetails(ByVal id As Long)
End Sub

Public Property Let Speciality(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Speciality = 5
    mvarSpeciality = vData
End Property


Public Property Get Speciality() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Speciality
    Speciality = mvarSpeciality
End Property



Public Property Let SpecialityID(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SpecialityID = 5
    mvarSpecialityID = vData
End Property


Public Property Get SpecialityID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SpecialityID
    SpecialityID = mvarSpecialityID
End Property



