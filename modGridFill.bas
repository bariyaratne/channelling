Attribute VB_Name = "modGridFill"
Option Explicit
    Dim temSQL As String
    
    
Public Function FillAnyGrid(InputSql As String, InputGrid As MSFlexGrid, TotalNameCol As Integer, TotalCols() As Integer, OmitRepeatCols() As Integer) As Double()
    Dim rsTem As New ADODB.Recordset
    Dim colTotal() As Double
    Dim previousValue() As String
    Dim i As Integer
    Dim col As Integer
    
    With rsTem
        If .State = 1 Then .Close
        temSQL = InputSql
        .Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
        
        InputGrid.Clear
        
        InputGrid.Rows = 1
        InputGrid.Cols = .Fields.Count
        
        ReDim colTotal(.Fields.Count)
        ReDim previousValue(.Fields.Count)
        
        InputGrid.Row = 0
                    
        For i = 0 To .Fields.Count - 1
            InputGrid.col = i
            InputGrid.Text = .Fields(i).Name
        Next i
        
        While .EOF = False
            InputGrid.Rows = InputGrid.Rows + 1
            InputGrid.Row = InputGrid.Rows - 1
            For i = 0 To .Fields.Count - 1
                InputGrid.col = i
                If UBound(OmitRepeatCols) > 0 Then
                    For col = 0 To UBound(OmitRepeatCols) - 1
                        If OmitRepeatCols(col) = i Then
                            If previousValue(i) <> .Fields(i).Value Then
                                previousValue(i) = .Fields(i).Value
                                InputGrid.Text = .Fields(i).Value
                            End If
                        Else
                            If IsNull(.Fields(i).Value) = False Then
                                InputGrid.Text = .Fields(i).Value
                            End If
                        End If
                    Next
                Else
                    If IsNull(.Fields(i).Value) = False Then
                        InputGrid.Text = .Fields(i).Value
                    End If
                End If
                For col = 0 To UBound(TotalCols) - 1
                    If TotalCols(col) = i Then
                        colTotal(i) = colTotal(i) + Val(.Fields(i).Value)
                    End If
                Next
            Next i
            .MoveNext
        Wend
        .Close
    End With
    
    If UBound(TotalCols) > 0 Then
        InputGrid.Rows = InputGrid.Rows + 1
        InputGrid.Row = InputGrid.Rows - 1
        InputGrid.col = TotalNameCol
        InputGrid.Text = "Total"
        For i = 0 To InputGrid.Cols - 1
            InputGrid.col = i
            For col = 0 To UBound(TotalCols) - 1
                If TotalCols(col) = i Then
                    InputGrid.Text = colTotal(i)
                End If
            Next
        Next i
    End If
    FillAnyGrid = colTotal
End Function


