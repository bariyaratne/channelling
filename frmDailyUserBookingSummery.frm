VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmDailyUserBookingSummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "User Bookings by Appointment Date"
   ClientHeight    =   11580
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   17310
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   11580
   ScaleWidth      =   17310
   Begin VB.CommandButton btnClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   15960
      TabIndex        =   6
      Top             =   11040
      Width           =   1215
   End
   Begin VB.CommandButton cmbExcel 
      Caption         =   "Excel"
      Height          =   375
      Left            =   1440
      TabIndex        =   5
      Top             =   11160
      Width           =   1215
   End
   Begin VB.CommandButton cmbPrint 
      Caption         =   "Print"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   11160
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid grid 
      Height          =   10335
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   17055
      _ExtentX        =   30083
      _ExtentY        =   18230
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   5400
      TabIndex        =   2
      Top             =   240
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker dtpAppDate 
      Height          =   375
      Left            =   1680
      TabIndex        =   1
      Top             =   240
      Width           =   3375
      _ExtentX        =   5953
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   154075139
      CurrentDate     =   41321
   End
   Begin VB.Label Label1 
      Caption         =   "Appointment Date"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   2415
   End
End
Attribute VB_Name = "frmDailyUserBookingSummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temTopic As String
    Dim temSubTopic As String
    
    Dim bookings As Long
        Dim settlings As Long
        Dim cancellation As Long
        Dim refunds As Long
        Dim docFee As Double
        Dim hosFee As Double
        Dim tax As Double

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnProcess_Click()
    processCash
    processCredit

End Sub



Private Sub processCash()


    With grid
        .Clear
        
        .Cols = 8
        .Rows = 1
        
        .Row = 0
        
        .col = 0
        .Text = "Date"
        
        .col = 1
        .Text = "User"
        
        .col = 2
        .Text = "Booked No"
        
        .col = 3
        .Text = "Cancellations/Refunds"
        
        .col = 4
        .Text = "Doctor Fee"
        
        .col = 5
        .Text = "Hospital Fee"
        
        .col = 6
        .Text = "Tax"
        
        .col = 7
        .Text = "Total"
    
    End With
    Dim rsTem As New ADODB.Recordset
    Dim rsSec As New ADODB.Recordset
    
    Dim temSQL As String
    With rsTem
    
        Dim TemMaxDate As Date
        Dim TemMinDate As Date
        
    
    
        temSQL = "SELECT Min(tblPatientFacility.BookingDate) AS MinOfBookingDate, Max(tblPatientFacility.BookingDate) AS MaxOfBookingDate FROM tblPatientFacility WHERE (((tblPatientFacility.AppointmentDate)=#" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "#)) "
       ' temWhere = " where (paymentmode = 'Cash' and bookingdate =#" & Format(DTPicker1.Value, "dd MMMM yyyy") & "#) or (paymentmode = 'Credit' and settlecashdate = #" & Format(DTPicker1.Value, "dd MMMM yyyy") & "#) or ( repaydate = #" & Format(DTPicker1.Value, "dd MMMM yyyy") & "#  ) "
        .Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
        If Not IsNull(!MaxOfBookingDate) Then
            TemMaxDate = !MaxOfBookingDate
        Else
            TemMaxDate = Format(dtpAppDate.Value, "dd MMMM yyyy")
        End If
        If Not IsNull(!MinOfBookingDate) Then
            TemMinDate = !MinOfBookingDate
        Else
            TemMinDate = Format(dtpAppDate.Value, "dd MMMM yyyy")
        End If
        
       
        Dim temnum As Long
        Dim TemDate As Date
        
        Dim totH As Double
        Dim totD As Double
        Dim totT As Double
        
       
        TemDate = TemMinDate
        
        
        While TemMinDate + temnum <= TemMaxDate
        
            Dim tembookings As Long
            Dim temSettlings As Long
            Dim temcancellation As Long
            Dim temrefunds As Long
            Dim TemDocFee As Double
            Dim temhosFee As Double
            Dim temtax As Double
            
            bookings = 0
            cancellation = 0
            refunds = 0
            docFee = 0
            hosFee = 0
            tax = 0
            
            Dim allBookings As New Collection
            Dim addingRow As StaffCollection
            
            
            
            TemDate = TemMinDate + temnum
            temnum = temnum + 1
            
            temSQL = "SELECT tblStaff.StaffName, tblPatientFacility.User_ID, Sum(tblPatientFacility.PersonalFee) AS SumOfPersonalFee, Sum(tblPatientFacility.InstitutionFee) AS SumOfInstitutionFee, Sum(tblPatientFacility.OtherFee) AS SumOfOtherFee, Count(tblPatientFacility.PatientFacility_ID) AS CountOfPatientFacility_ID " & _
                        "FROM tblPatientFacility LEFT JOIN tblStaff ON tblPatientFacility.User_ID = tblStaff.Staff_ID " & _
                        "Where (((tblPatientFacility.PaymentMode) = 'Cash') And ((tblPatientFacility.BookingDate) = #" & Format(TemDate, "dd MMMM yyyy") & "#) And ((tblPatientFacility.AppointmentDate) = #" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "#) And ((tblPatientFacility.FullyPaid) = True)) " & _
                        "GROUP BY tblStaff.StaffName,  tblPatientFacility.User_ID " & _
                        "ORDER BY tblStaff.StaffName"
            If .State = 1 Then .Close
            .Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
            
            
            
            While .EOF = False
            
                If IsNull(!CountOfPatientFacility_ID) = False Then tembookings = !CountOfPatientFacility_ID
                If IsNull(!SumOfPersonalFee) = False Then docFee = !SumOfPersonalFee
                If IsNull(!SumOfInstitutionFee) = False Then hosFee = !SumOfInstitutionFee
                If IsNull(!sumofOtherFee) = False Then tax = !sumofOtherFee
                 
                temSQL = "SELECT Sum(tblPatientFacility.PersonalFee) AS SumOfPersonalFee, Sum(tblPatientFacility.InstitutionFee) AS SumOfInstitutionFee, Sum(tblPatientFacility.OtherFee) AS SumOfOtherFee, Count(tblPatientFacility.PatientFacility_ID) AS CountOfPatientFacility_ID " & _
                            "From tblPatientFacility " & _
                            "WHERE (((tblPatientFacility.PaymentMode)='Credit') And ((tblPatientFacility.SettleCashDate) = #" & Format(TemDate, "dd MMMM yyyy") & "#) And ((tblPatientFacility.AppointmentDate) = #" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "#) And  ((tblPatientFacility.FullyPaid)=True) AND ((tblPatientFacility.CreditSettleUser_ID)=" & !User_ID & "))"

                If rsSec.State = 1 Then rsSec.Close
                rsSec.Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
                
                If IsNull(rsSec!SumOfPersonalFee) = False Then docFee = docFee + rsSec!SumOfPersonalFee
                If IsNull(rsSec!SumOfInstitutionFee) = False Then hosFee = hosFee + rsSec!SumOfInstitutionFee
                If IsNull(rsSec!sumofOtherFee) = False Then tax = tax + rsSec!sumofOtherFee
            
                temSQL = "SELECT Sum(tblPatientFacility.PersonalRefund) AS SumOfPersonalRefund, Sum(tblPatientFacility.InstitutionRefund) AS SumOfInstitutionRefund, Sum(tblPatientFacility.OtherRefund) AS SumOfOtherRefund, Count(tblPatientFacility.PatientFacility_ID) AS CountOfPatientFacility_ID " & _
                            "From tblPatientFacility " & _
                            "WHERE ((((tblPatientFacility.PaymentMode)='Cash')  OR ((tblPatientFacility.PaymentMode)='Credit')) AND ((tblPatientFacility.RepayDate)=#" & Format(TemDate, "dd MMMM yyyy") & "#) AND ((tblPatientFacility.AppointmentDate)=#" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "#)  AND ((tblPatientFacility.RepayUser_ID)= " & !User_ID & ") AND  ((tblPatientFacility.FullyPaid)=True))"
        
        
          temSQL = "SELECT Sum(tblPatientFacility.PersonalRefund) AS SumOfPersonalRefund, Sum(tblPatientFacility.InstitutionRefund) AS SumOfInstitutionRefund, Sum(tblPatientFacility.OtherRefund) AS SumOfOtherRefund, Count(tblPatientFacility.PatientFacility_ID) AS CountOfPatientFacility_ID " & _
                            "From tblPatientFacility " & _
                            "WHERE tblPatientFacility.RepayDate=#" & Format(TemDate, "dd MMMM yyyy") & "# AND tblPatientFacility.AppointmentDate=#" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "# AND tblPatientFacility.RepayUser_ID= " & !User_ID & " AND tblPatientFacility.FullyPaid=True"
        
        
                If rsSec.State = 1 Then rsSec.Close
                rsSec.Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
        
                If IsNull(rsSec!SumOfPersonalRefund) = False Then docFee = docFee - rsSec!SumOfPersonalRefund
                If IsNull(rsSec!SumOfInstitutionRefund) = False Then hosFee = hosFee - rsSec!SumOfInstitutionRefund
                If IsNull(rsSec!SumOfOtherRefund) = False Then tax = tax - rsSec!SumOfOtherRefund
                If IsNull(rsSec!CountOfPatientFacility_ID) = False Then cancellation = rsSec!CountOfPatientFacility_ID
                
                
                grid.Rows = grid.Rows + 1
                grid.Row = grid.Rows - 1
                
                grid.col = 0
                grid.Text = Format(TemDate, "dd MMMM yyyy")
                
                grid.col = 1
                If IsNull(!StaffName) = False Then grid.Text = !StaffName
                
                grid.col = 2
                grid.Text = tembookings
                
                grid.col = 3
                grid.Text = cancellation
                 
                grid.col = 4
                grid.Text = Format(docFee, "#,##0.00")
                
                grid.col = 5
                grid.Text = Format(hosFee, "#,##0.00")
                
                grid.col = 6
                grid.Text = Format(tax, "#,##0.00")
                
                grid.col = 7
                grid.Text = Format(docFee + hosFee + tax, "#,##0.00")
                
                totH = totH + hosFee
                totD = totD + docFee
                totT = totT + tax
                        
                        
                        
                .MoveNext
            Wend
            .Close
            
            
           
            
            
        Wend
        
        
         grid.Rows = grid.Rows + 1
            grid.Row = grid.Rows - 1
            
            grid.col = 0
            grid.Text = "Total"
           
            grid.col = 4
            grid.Text = Format(totD, "#,##0.00")
            
            grid.col = 5
            grid.Text = Format(totH, "#,##0.00")
            
            grid.col = 6
            grid.Text = Format(totT, "#,##0.00")
            
            grid.col = 7
            grid.Text = Format(totD + totH + totT, "#,##0.00")
            
            
            grid.ColWidth(0) = 2000
             grid.ColWidth(1) = 2600
             
    End With

End Sub


Private Sub processCredit()


    With grid
        .Clear
        
        .Cols = 8
        .Rows = 1
        
        .Row = 0
        
        .col = 0
        .Text = "Date"
        
        .col = 1
        .Text = "User"
        
        .col = 2
        .Text = "Booked No"
        
        .col = 3
        .Text = "Cancellations/Refunds"
        
        .col = 4
        .Text = "Doctor Fee"
        
        .col = 5
        .Text = "Hospital Fee"
        
        .col = 6
        .Text = "Tax"
        
        .col = 7
        .Text = "Total"
    
    End With
    Dim rsTem As New ADODB.Recordset
    Dim rsSec As New ADODB.Recordset
    
    Dim temSQL As String
    With rsTem
    
        Dim TemMaxDate As Date
        Dim TemMinDate As Date
        
    
    
        temSQL = "SELECT Min(tblPatientFacility.BookingDate) AS MinOfBookingDate, Max(tblPatientFacility.BookingDate) AS MaxOfBookingDate FROM tblPatientFacility WHERE (((tblPatientFacility.AppointmentDate)=#" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "#)) "
       ' temWhere = " where (paymentmode = 'Cash' and bookingdate =#" & Format(DTPicker1.Value, "dd MMMM yyyy") & "#) or (paymentmode = 'Credit' and settlecashdate = #" & Format(DTPicker1.Value, "dd MMMM yyyy") & "#) or ( repaydate = #" & Format(DTPicker1.Value, "dd MMMM yyyy") & "#  ) "
        .Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
        If Not IsNull(!MaxOfBookingDate) Then
            TemMaxDate = !MaxOfBookingDate
        Else
            TemMaxDate = Format(dtpAppDate.Value, "dd MMMM yyyy")
        End If
        If Not IsNull(!MinOfBookingDate) Then
            TemMinDate = !MinOfBookingDate
        Else
            TemMinDate = Format(dtpAppDate.Value, "dd MMMM yyyy")
        End If
        
        
        Dim temnum As Long
        Dim TemDate As Date
        
        Dim totH As Double
        Dim totD As Double
        Dim totT As Double
        
       
        TemDate = TemMinDate
        
        
        While TemMinDate + temnum <= TemMaxDate
        
            Dim tembookings As Long
            Dim temSettlings As Long
            Dim temcancellation As Long
            Dim temrefunds As Long
            Dim TemDocFee As Double
            Dim temhosFee As Double
            Dim temtax As Double
            
          
            
            Dim allBookings As New Collection
            Dim addingRow As StaffCollection
            
            
            
            TemDate = TemMinDate + temnum
            temnum = temnum + 1
            
            temSQL = "SELECT tblStaff.StaffName, tblPatientFacility.CreditSettleUser_ID, Sum(tblPatientFacility.PersonalFee) AS SumOfPersonalFee, Sum(tblPatientFacility.InstitutionFee) AS SumOfInstitutionFee, Sum(tblPatientFacility.OtherFee) AS SumOfOtherFee, Count(tblPatientFacility.PatientFacility_ID) AS CountOfPatientFacility_ID " & _
                        "FROM tblPatientFacility LEFT JOIN tblStaff ON tblPatientFacility.CreditSettleUser_ID = tblStaff.Staff_ID " & _
                        "Where ((((tblPatientFacility.PaymentMode) = 'Credit') And ((tblPatientFacility.SettleCashDate) = #" & Format(TemDate, "dd MMMM yyyy") & "#) And ((tblPatientFacility.AppointmentDate) = #" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "#) And ((tblPatientFacility.FullyPaid) = True))) " & _
                        "GROUP BY tblStaff.StaffName,  tblPatientFacility.CreditSettleUser_ID " & _
                        "ORDER BY tblStaff.StaffName"
            If .State = 1 Then .Close
            .Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
            
            
            
            While .EOF = False
            
                If IsNull(!CountOfPatientFacility_ID) = False Then tembookings = !CountOfPatientFacility_ID
                If IsNull(!SumOfPersonalFee) = False Then docFee = !SumOfPersonalFee
                If IsNull(!SumOfInstitutionFee) = False Then hosFee = !SumOfInstitutionFee
                If IsNull(!sumofOtherFee) = False Then tax = !sumofOtherFee
                 
                temSQL = "SELECT Sum(tblPatientFacility.PersonalFee) AS SumOfPersonalFee, Sum(tblPatientFacility.InstitutionFee) AS SumOfInstitutionFee, Sum(tblPatientFacility.OtherFee) AS SumOfOtherFee, Count(tblPatientFacility.PatientFacility_ID) AS CountOfPatientFacility_ID " & _
                            "From tblPatientFacility " & _
                            "WHERE (((tblPatientFacility.PaymentMode)='Credit') And ((tblPatientFacility.SettleCashDate) = #" & Format(TemDate, "dd MMMM yyyy") & "#) And ((tblPatientFacility.AppointmentDate) = #" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "#) And  ((tblPatientFacility.FullyPaid)=True) AND ((tblPatientFacility.CreditSettleUser_ID)=" & !CreditSettleUser_ID & "))"

                If rsSec.State = 1 Then rsSec.Close
                rsSec.Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
                
                If IsNull(rsSec!SumOfPersonalFee) = False Then docFee = docFee + rsSec!SumOfPersonalFee
                If IsNull(rsSec!SumOfInstitutionFee) = False Then hosFee = hosFee + rsSec!SumOfInstitutionFee
                If IsNull(rsSec!sumofOtherFee) = False Then tax = tax + rsSec!sumofOtherFee
            
                temSQL = "SELECT Sum(tblPatientFacility.PersonalRefund) AS SumOfPersonalRefund, Sum(tblPatientFacility.InstitutionRefund) AS SumOfInstitutionRefund, Sum(tblPatientFacility.OtherRefund) AS SumOfOtherRefund, Count(tblPatientFacility.PatientFacility_ID) AS CountOfPatientFacility_ID " & _
                            "From tblPatientFacility " & _
                            "WHERE ((((tblPatientFacility.PaymentMode)='Cash')  OR ((tblPatientFacility.PaymentMode)='Credit')) AND ((tblPatientFacility.RepayDate)=#" & Format(TemDate, "dd MMMM yyyy") & "#) AND ((tblPatientFacility.AppointmentDate)=#" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "#)  AND ((tblPatientFacility.RepayUser_ID)= " & !CreditSettleUser_ID & ") AND  ((tblPatientFacility.FullyPaid)=True))"
        
        
          temSQL = "SELECT Sum(tblPatientFacility.PersonalRefund) AS SumOfPersonalRefund, Sum(tblPatientFacility.InstitutionRefund) AS SumOfInstitutionRefund, Sum(tblPatientFacility.OtherRefund) AS SumOfOtherRefund, Count(tblPatientFacility.PatientFacility_ID) AS CountOfPatientFacility_ID " & _
                            "From tblPatientFacility " & _
                            "WHERE tblPatientFacility.RepayDate=#" & Format(TemDate, "dd MMMM yyyy") & "# AND tblPatientFacility.AppointmentDate=#" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "# AND tblPatientFacility.RepayUser_ID= " & !CreditSettleUser_ID & " AND tblPatientFacility.FullyPaid=True"
        
        
                If rsSec.State = 1 Then rsSec.Close
                rsSec.Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
        
                If IsNull(rsSec!SumOfPersonalRefund) = False Then docFee = docFee - rsSec!SumOfPersonalRefund
                If IsNull(rsSec!SumOfInstitutionRefund) = False Then hosFee = hosFee - rsSec!SumOfInstitutionRefund
                If IsNull(rsSec!SumOfOtherRefund) = False Then tax = tax - rsSec!SumOfOtherRefund
                If IsNull(rsSec!CountOfPatientFacility_ID) = False Then cancellation = rsSec!CountOfPatientFacility_ID
                
                
                
                Dim r As Integer
                Dim c As Integer
                
                Dim i As Integer
                Dim found As Boolean
                
                found = False
                
                For i = 1 To grid.Rows - 1
                    If Format(TemDate, "dd MMMM yyyy") = grid.TextMatrix(i, 0) And !StaffName = grid.TextMatrix(i, 1) Then
                    
                        found = True
                    End If
                Next
                
                
                grid.Rows = grid.Rows + 1
                grid.Row = grid.Rows - 1
                
                grid.col = 0
                grid.Text = Format(TemDate, "dd MMMM yyyy")
                
                grid.col = 1
                If IsNull(!StaffName) = False Then grid.Text = !StaffName
                
                grid.col = 2
                grid.Text = tembookings
                
                grid.col = 3
                grid.Text = cancellation
                 
                grid.col = 4
                grid.Text = Format(docFee, "#,##0.00")
                
                grid.col = 5
                grid.Text = Format(hosFee, "#,##0.00")
                
                grid.col = 6
                grid.Text = Format(tax, "#,##0.00")
                
                grid.col = 7
                grid.Text = Format(docFee + hosFee + tax, "#,##0.00")
                
                totH = totH + hosFee
                totD = totD + docFee
                totT = totT + tax
                        
                        
                        
                .MoveNext
            Wend
            .Close
            
            
           
            
            
        Wend
        
        
         grid.Rows = grid.Rows + 1
            grid.Row = grid.Rows - 1
            
            grid.col = 0
            grid.Text = "Total"
           
            grid.col = 4
            grid.Text = Format(totD, "#,##0.00")
            
            grid.col = 5
            grid.Text = Format(totH, "#,##0.00")
            
            grid.col = 6
            grid.Text = Format(totT, "#,##0.00")
            
            grid.col = 7
            grid.Text = Format(totD + totH + totT, "#,##0.00")
            
            
            grid.ColWidth(0) = 2000
             grid.ColWidth(1) = 2600
             
    End With

End Sub

Private Sub cmbExcel_Click()
   
     temTopic = "Daily Cash Bookins For Appointment Day"
    temSubTopic = "Appointment Date " & Format(dtpAppDate.Value, "dd MMM yyyy")
     GridToExcel grid, temTopic, temSubTopic
End Sub

Private Sub cmbPrint_Click()
        On Error Resume Next

    Dim SetPrinter As New cSetDfltPrinter
    
    SetPrinter.SetPrinterAsDefault ""
    
    Dim MyPrinter As Printer
    
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = ReportPrinterName Then
            Set Printer = MyPrinter
        End If
    Next
    
    temTopic = "Daily Cash Bookins For Appointment Day"
    temSubTopic = "Appointment Date " & Format(dtpAppDate.Value, "dd MMM yyyy")
    
    SetPrinter.SetPrinterAsDefault ReportPrinterName
    
    Dim myPR As GridPrintSettings
    
    myPR = getDefaultGridPrint
    
    
    
    myPR.Topic = temTopic
    myPR.Subtopic = temSubTopic
    
    PrintGrid grid, myPR
    
End Sub

Private Sub Form_Load()
    dtpAppDate.Value = Date
End Sub
