VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Agent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varInstitution_ID As Long
    Private varInstitutionName As String
    Private varInstitutionAddress As String
    Private varInstitutionTelephone As String
    Private varInstitutionFax As String
    Private varInstitutionEmail As String
    Private varInstitutionComments As String
    Private varInstitutionPaymentMethod_ID As String
    Private varInstitutionBank_ID As Long
    Private varInstitutionBankBranch As String
    Private varInstitutionAccount As String
    Private varInstitutionCredit As Double
    Private varInstitutionIsAnAgent As Boolean
    Private varInstitutionBlackListed As Boolean
    Private varInstitutionMaxCredit As Double
    Private varInstitutionCode As String
    Private varToday As Date
    Private varCashAgent As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblInstitutions"
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If varInstitution_ID = 0 Then .AddNew
        !InstitutionName = varInstitutionName
        !InstitutionAddress = varInstitutionAddress
        !InstitutionTelephone = varInstitutionTelephone
        !InstitutionFax = varInstitutionFax
        !InstitutionEmail = varInstitutionEmail
        !InstitutionComments = varInstitutionComments
        !InstitutionPaymentMethod_ID = varInstitutionPaymentMethod_ID
        !InstitutionBank_ID = varInstitutionBank_ID
        !InstitutionBankBranch = varInstitutionBankBranch
        !InstitutionAccount = varInstitutionAccount
        !InstitutionCredit = varInstitutionCredit
        !InstitutionIsAnAgent = varInstitutionIsAnAgent
        !InstitutionBlackListed = varInstitutionBlackListed
        !InstitutionMaxCredit = varInstitutionMaxCredit
        !InstitutionCode = varInstitutionCode
        !Today = varToday
        !CashAgent = varCashAgent
        .Update
        varInstitution_ID = !Institution_ID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblInstitutions WHERE Institution_ID = " & varInstitution_ID
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If Not IsNull(!Institution_ID) Then
           varInstitution_ID = !Institution_ID
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionName = !InstitutionName
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionAddress = !InstitutionAddress
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionTelephone = !InstitutionTelephone
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionFax = !InstitutionFax
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionEmail = !InstitutionEmail
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionComments = !InstitutionComments
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionPaymentMethod_ID = !InstitutionPaymentMethod_ID
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionBank_ID = !InstitutionBank_ID
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionBankBranch = !InstitutionBankBranch
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionAccount = !InstitutionAccount
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionCredit = !InstitutionCredit
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionIsAnAgent = !InstitutionIsAnAgent
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionBlackListed = !InstitutionBlackListed
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionMaxCredit = !InstitutionMaxCredit
        End If
        If Not IsNull(!Institution_ID) Then
           varInstitutionCode = !InstitutionCode
        End If
        If Not IsNull(!Institution_ID) Then
           varToday = !Today
        End If
        If Not IsNull(!Institution_ID) Then
           varCashAgent = !CashAgent
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varInstitution_ID = 0
    varInstitutionName = Empty
    varInstitutionAddress = Empty
    varInstitutionTelephone = Empty
    varInstitutionFax = Empty
    varInstitutionEmail = Empty
    varInstitutionComments = Empty
    varInstitutionPaymentMethod_ID = Empty
    varInstitutionBank_ID = 0
    varInstitutionBankBranch = Empty
    varInstitutionAccount = Empty
    varInstitutionCredit = 0
    varInstitutionIsAnAgent = False
    varInstitutionBlackListed = False
    varInstitutionMaxCredit = 0
    varInstitutionCode = Empty
    varToday = Empty
    varCashAgent = False
End Sub

Public Property Let Institution_ID(ByVal vInstitution_ID As Long)
    Call clearData
    varInstitution_ID = vInstitution_ID
    Call loadData
End Property

Public Property Get Institution_ID() As Long
    Institution_ID = varInstitution_ID
End Property

Public Property Let InstitutionName(ByVal vInstitutionName As String)
    varInstitutionName = vInstitutionName
End Property

Public Property Get InstitutionName() As String
    InstitutionName = varInstitutionName
End Property

Public Property Let InstitutionAddress(ByVal vInstitutionAddress As String)
    varInstitutionAddress = vInstitutionAddress
End Property

Public Property Get InstitutionAddress() As String
    InstitutionAddress = varInstitutionAddress
End Property

Public Property Let InstitutionTelephone(ByVal vInstitutionTelephone As String)
    varInstitutionTelephone = vInstitutionTelephone
End Property

Public Property Get InstitutionTelephone() As String
    InstitutionTelephone = varInstitutionTelephone
End Property

Public Property Let InstitutionFax(ByVal vInstitutionFax As String)
    varInstitutionFax = vInstitutionFax
End Property

Public Property Get InstitutionFax() As String
    InstitutionFax = varInstitutionFax
End Property

Public Property Let InstitutionEmail(ByVal vInstitutionEmail As String)
    varInstitutionEmail = vInstitutionEmail
End Property

Public Property Get InstitutionEmail() As String
    InstitutionEmail = varInstitutionEmail
End Property

Public Property Let InstitutionComments(ByVal vInstitutionComments As String)
    varInstitutionComments = vInstitutionComments
End Property

Public Property Get InstitutionComments() As String
    InstitutionComments = varInstitutionComments
End Property

Public Property Let InstitutionPaymentMethod_ID(ByVal vInstitutionPaymentMethod_ID As String)
    varInstitutionPaymentMethod_ID = vInstitutionPaymentMethod_ID
End Property

Public Property Get InstitutionPaymentMethod_ID() As String
    InstitutionPaymentMethod_ID = varInstitutionPaymentMethod_ID
End Property

Public Property Let InstitutionBank_ID(ByVal vInstitutionBank_ID As Long)
    varInstitutionBank_ID = vInstitutionBank_ID
End Property

Public Property Get InstitutionBank_ID() As Long
    InstitutionBank_ID = varInstitutionBank_ID
End Property

Public Property Let InstitutionBankBranch(ByVal vInstitutionBankBranch As String)
    varInstitutionBankBranch = vInstitutionBankBranch
End Property

Public Property Get InstitutionBankBranch() As String
    InstitutionBankBranch = varInstitutionBankBranch
End Property

Public Property Let InstitutionAccount(ByVal vInstitutionAccount As String)
    varInstitutionAccount = vInstitutionAccount
End Property

Public Property Get InstitutionAccount() As String
    InstitutionAccount = varInstitutionAccount
End Property

Public Property Let InstitutionCredit(ByVal vInstitutionCredit As Double)
    varInstitutionCredit = vInstitutionCredit
End Property

Public Property Get InstitutionCredit() As Double
    InstitutionCredit = varInstitutionCredit
End Property

Public Property Let InstitutionIsAnAgent(ByVal vInstitutionIsAnAgent As Boolean)
    varInstitutionIsAnAgent = vInstitutionIsAnAgent
End Property

Public Property Get InstitutionIsAnAgent() As Boolean
    InstitutionIsAnAgent = varInstitutionIsAnAgent
End Property

Public Property Let InstitutionBlackListed(ByVal vInstitutionBlackListed As Boolean)
    varInstitutionBlackListed = vInstitutionBlackListed
End Property

Public Property Get InstitutionBlackListed() As Boolean
    InstitutionBlackListed = varInstitutionBlackListed
End Property

Public Property Let InstitutionMaxCredit(ByVal vInstitutionMaxCredit As Double)
    varInstitutionMaxCredit = vInstitutionMaxCredit
End Property

Public Property Get InstitutionMaxCredit() As Double
    InstitutionMaxCredit = varInstitutionMaxCredit
End Property

Public Property Let InstitutionCode(ByVal vInstitutionCode As String)
    varInstitutionCode = vInstitutionCode
End Property

Public Property Get InstitutionCode() As String
    InstitutionCode = varInstitutionCode
End Property

Public Property Let Today(ByVal vToday As Date)
    varToday = vToday
End Property

Public Property Get Today() As Date
    Today = varToday
End Property

Public Property Let CashAgent(ByVal vCashAgent As Boolean)
    varCashAgent = vCashAgent
End Property

Public Property Get CashAgent() As Boolean
    CashAgent = varCashAgent
End Property


