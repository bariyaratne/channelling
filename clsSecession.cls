VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSecession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarSecessionID As Long 'local copy
Private mvarConsultant As clsConsultant 'local copy
Private mvarSecessionName As String 'local copy
Private mvarSecessionWeekday As Long 'local copy
Private mvarfaullDayLeave As Boolean 'local copy
Private mvaralteredDate As Date 'local copy
Private mvarStartingTime As Date 'local copy
Private mvarUsualDuration As Long 'local copy
Private mvarStatingNo As Integer 'local copy
Private mvarStartingNo As Integer 'local copy
Private mvarIncreaseNo As Integer 'local copy
Private mvarIncreaseMin As Integer 'local copy
Private mvarMaximum As Integer 'local copy
Private mvarLocalDoctorFee As Double 'local copy
Private mvarLocalHospitalFee As Double 'local copy
Private mvarLocalTax As Double 'local copy
Private mvarForeignDoctorFee As Double 'local copy
Private mvarForeignHospitalFee As Double 'local copy
Private mvarForeignTax As Double 'local copy
Private mvarAgentDoctorFee As Double 'local copy
Private mvarAgentHospitalFee As Double 'local copy
Private mvarAgentTax As Double 'local copy
Private mvarCanBypassOrder As Boolean 'local copy
Private mvarCalculateAppointmnt As Boolean 'local copy
Private mvarComments As String 'local copy
Private mvarRoomNo As String 'local copy

Dim temSQL As String
'local variable(s) to hold property value(s)
Private mvarHospitalFacility As Long 'local copy
'local variable(s) to hold property value(s)
Private mvarStaffID As Long 'local copy
'local variable(s) to hold property value(s)
Private mvarChangeDate As Date 'local copy
Public Property Let ChangeDate(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ChangeDate = 5
    mvarChangeDate = vData
End Property


Public Property Get ChangeDate() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ChangeDate
    ChangeDate = mvarChangeDate
End Property



Public Property Let staffId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.StaffID = 5
    mvarStaffID = vData
End Property


Public Property Get staffId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.StaffID
    staffId = mvarStaffID
End Property



Public Property Let HospitalFacility(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.HospitalFacility = 5
    mvarHospitalFacility = vData
End Property


Public Property Get HospitalFacility() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.HospitalFacility
    HospitalFacility = mvarHospitalFacility
End Property




Public Property Let RoomNo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.RoomNo = 5
    mvarRoomNo = vData
End Property


Public Property Get RoomNo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.RoomNo
    RoomNo = mvarRoomNo
End Property



Public Property Let Comments(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Comments = 5
    mvarComments = vData
End Property


Public Property Get Comments() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Comments
    Comments = mvarComments
End Property



Public Property Let CalculateAppointmnt(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CalculateAppointmnt = 5
    mvarCalculateAppointmnt = vData
End Property


Public Property Get CalculateAppointmnt() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CalculateAppointmnt
    CalculateAppointmnt = mvarCalculateAppointmnt
End Property



Public Property Let CanByPassOrder(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CanBypassOrder = 5
    mvarCanBypassOrder = vData
End Property


Public Property Get CanByPassOrder() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CanBypassOrder
    CanByPassOrder = mvarCanBypassOrder
End Property



Public Property Let AgentTax(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.AgentTax = 5
    mvarAgentTax = vData
End Property


Public Property Get AgentTax() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.AgentTax
    AgentTax = mvarAgentTax
End Property



Public Property Let AgentHospitalFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.AgentHospitalFee = 5
    mvarAgentHospitalFee = vData
End Property


Public Property Get AgentHospitalFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.AgentHospitalFee
    AgentHospitalFee = mvarAgentHospitalFee
End Property



Public Property Let AgentDoctorFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.AgentDoctorFee = 5
    mvarAgentDoctorFee = vData
End Property


Public Property Get AgentDoctorFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.AgentDoctorFee
    AgentDoctorFee = mvarAgentDoctorFee
End Property



Public Property Let ForeignTax(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ForeignTax = 5
    mvarForeignTax = vData
End Property


Public Property Get ForeignTax() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ForeignTax
    ForeignTax = mvarForeignTax
End Property



Public Property Let ForeignHospitalFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ForeignHospitalFee = 5
    mvarForeignHospitalFee = vData
End Property


Public Property Get ForeignHospitalFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ForeignHospitalFee
    ForeignHospitalFee = mvarForeignHospitalFee
End Property



Public Property Let ForeignDoctorFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ForeignDoctorFee = 5
    mvarForeignDoctorFee = vData
End Property


Public Property Get ForeignDoctorFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ForeignDoctorFee
    ForeignDoctorFee = mvarForeignDoctorFee
End Property



Public Property Let LocalTax(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LocalTax = 5
    mvarLocalTax = vData
End Property


Public Property Get LocalTax() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LocalTax
    LocalTax = mvarLocalTax
End Property



Public Property Let LocalHospitalFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LocalHospitalFee = 5
    mvarLocalHospitalFee = vData
End Property


Public Property Get LocalHospitalFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LocalHospitalFee
    LocalHospitalFee = mvarLocalHospitalFee
End Property



Public Property Let LocalDoctorFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LocalDoctorFee = 5
    mvarLocalDoctorFee = vData
End Property


Public Property Get LocalDoctorFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LocalDoctorFee
    LocalDoctorFee = mvarLocalDoctorFee
End Property



Public Property Let Maximum(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Maximum = 5
    mvarMaximum = vData
End Property


Public Property Get Maximum() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Maximum
    Maximum = mvarMaximum
End Property



Public Property Let IncreaseMin(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IncreaseMin = 5
    mvarIncreaseMin = vData
End Property


Public Property Get IncreaseMin() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IncreaseMin
    IncreaseMin = mvarIncreaseMin
End Property



Public Property Let IncreaseNo(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IncreaseNo = 5
    mvarIncreaseNo = vData
End Property


Public Property Get IncreaseNo() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IncreaseNo
    IncreaseNo = mvarIncreaseNo
End Property



Public Property Let StartingNo(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.StartingNo = 5
    mvarStartingNo = vData
End Property


Public Property Get StartingNo() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.StartingNo
    StartingNo = mvarStartingNo
End Property



Public Property Let StatingNo(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.StatingNo = 5
    mvarStatingNo = vData
End Property


Public Property Get StatingNo() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.StatingNo
    StatingNo = mvarStatingNo
End Property



Public Property Let UsualDuration(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.UsualDuration = 5
    mvarUsualDuration = vData
End Property


Public Property Get UsualDuration() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.UsualDuration
    UsualDuration = mvarUsualDuration
End Property



Public Property Let StartingTime(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.StartingTime = 5
    mvarStartingTime = vData
End Property


Public Property Get StartingTime() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.StartingTime
    StartingTime = mvarStartingTime
End Property



Public Property Let AlteredDate(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.alteredDate = 5
    mvaralteredDate = vData
End Property


Public Property Get AlteredDate() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.alteredDate
    AlteredDate = mvaralteredDate
End Property



Public Property Let faullDayLeave(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.faullDayLeave = 5
    mvarfaullDayLeave = vData
End Property


Public Property Get faullDayLeave() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.faullDayLeave
    faullDayLeave = mvarfaullDayLeave
End Property



Public Property Let SecessionWeekday(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SecessionWeekday = 5
    mvarSecessionWeekday = vData
End Property


Public Property Get SecessionWeekday() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SecessionWeekday
    SecessionWeekday = mvarSecessionWeekday
End Property



Public Property Let SecessionName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SecessionName = 5
    mvarSecessionName = vData
End Property


Public Property Get SecessionName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SecessionName
    SecessionName = mvarSecessionName
End Property



Public Property Set Consultant(ByVal vData As clsConsultant)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Consultant = Form1
    Set mvarConsultant = vData
End Property


Public Property Get Consultant() As clsConsultant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Consultant
    Set Consultant = mvarConsultant
End Property



Public Property Let SecessionID(ByVal vData As Long)
    mvarSecessionID = vData
    
    mvarStaffID = Empty
    mvarSecessionName = Empty
    mvarStartingTime = Empty
    mvarUsualDuration = Empty
    mvarMaximum = Empty
    mvarLocalDoctorFee = Empty
    mvarLocalHospitalFee = Empty
    mvarLocalTax = Empty
    mvarForeignTax = Empty
    mvarForeignDoctorFee = Empty
    mvarForeignHospitalFee = Empty
    mvarAgentTax = Empty
    mvarAgentDoctorFee = Empty
    mvarAgentHospitalFee = Empty
    mvarCanBypassOrder = Empty
    mvarSecessionWeekday = Empty
    mvarChangeDate = Empty
    mvarfaullDayLeave = Empty
    mvarCalculateAppointmnt = Empty
    mvarComments = Empty
    mvaralteredDate = Empty
    mvarRoomNo = Empty
    mvarStartingNo = Empty
    mvarIncreaseNo = Empty
    mvarIncreaseMin = Empty
    mvarHospitalFacility = Empty
    
    
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT tblFacilitySecession.FacilitySecession_ID, " & _
            "tblFacilitySecession.FacilityStaff_ID , " & _
            "tblFacilitySecession.Staff_ID , " & _
            "tblFacilitySecession.SecessionName , " & _
            "tblFacilitySecession.StartingTime , " & _
            "tblFacilitySecession.UsualDuration , " & _
            "tblFacilitySecession.Maximum , " & _
            "tblFacilitySecession.LocalDoctorFee , " & _
            "tblFacilitySecession.LocalTax , " & _
            "tblFacilitySecession.ForeignTax , " & _
            "tblFacilitySecession.AgentTax , " & _
            "tblFacilitySecession.ForeignDoctorFee , " & _
            "tblFacilitySecession.AgentDoctorFee , " & _
            "tblFacilitySecession.LocalHospitalFee , " & _
            "tblFacilitySecession.ForeignHospitalFee , " & _
            "tblFacilitySecession.AgentHospitalFee , " & _
            "tblFacilitySecession.CanBypassOrder , " & _
            "tblFacilitySecession.SecessionWeekday , " & _
            "tblFacilitySecession.ChangeDate , "
            temSQL = temSQL + "tblFacilitySecession.FullDayLeave , " & _
            "tblFacilitySecession.CalculateAppointment , " & _
            "tblFacilitySecession.Comments , " & _
            "tblFacilitySecession.alteredDate , " & _
            "tblFacilitySecession.RoomNo , " & _
            "tblFacilitySecession.StartingNo , " & _
            "tblFacilitySecession.IncreaseNo , " & _
            "tblFacilitySecession.IncreaseMin , " & _
            "tblFacilitySecession.HospitalFacility_ID , " & _
            "From tblFacilitySecession " & _
            "Where (((tblFacilitySecession.FacilitySecession_ID) = " & vData & "))"
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            mvarStaffID = !Staff_ID
            mvarSecessionName = !SecessionName
            mvarStartingTime = !StartingTime
            mvarUsualDuration = !UsualDuration
            mvarMaximum = !Maximum
            mvarLocalDoctorFee = !LocalDoctorFee
            mvarLocalHospitalFee = !LocalHospitalFee
            mvarLocalTax = !LocalTax
            mvarForeignTax = !ForeignTax
            mvarForeignDoctorFee = !ForeignDoctorFee
            mvarForeignHospitalFee = !ForeignHospitalFee
            mvarAgentTax = !AgentTax
            mvarAgentDoctorFee = !AgentDoctorFee
            mvarAgentHospitalFee = !AgentHospitalFee
            mvarCanBypassOrder = !CanByPassOrder
            mvarSecessionWeekday = !SecessionWeekday
            mvarChangeDate = !ChangeDate
            mvarfaullDayLeave = !FullDayLeave
            mvarCalculateAppointmnt = !CalculateAppointment
            mvarComments = !Comments
            mvaralteredDate = !AlteredDate
            mvarRoomNo = !RoomNo
            mvarStartingNo = !StartingNo
            mvarIncreaseNo = !IncreaseNo
            mvarIncreaseMin = !IncreaseMin
            mvarHospitalFacility = !HospitalFacility_ID
        End If
        
        
    End With
End Property


Public Property Get SecessionID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SecessionID
    SecessionID = mvarSecessionID
End Property



