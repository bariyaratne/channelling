Attribute VB_Name = "ModuleReportPrinting"
Option Explicit
    Public Enum TextAlignment
        LeftAlign = 0
        RightAlign = 1
        CenterAlign = 2
        Justified = 3
    End Enum
    
Public Sub PrintReportText(ByVal SuppliedText As String, X1 As Long, X2 As Long, Y1 As Long, Y2 As Long, PrintFontName As String, PrintFontSize As Long, PrintFontBold As Boolean, PrintFontItalic As Boolean, PrintFontUnderline As Boolean, PrintFontStrikethrough As Boolean, PrintAlignment As Integer)
    Dim NewString As String
    Dim PrintString As String
    Dim COuntUp As Long
    Dim UpPosition As Long
    Dim UpSpacePosition As Long
    Dim NewStringLength As Long
    Dim PrintedString As String
    Dim MyX1 As Single
    
    MyX1 = X1
    
    NewString = SuppliedText
    PrintString = SuppliedText
    
    Printer.Font.Name = PrintFontName
    Printer.Font.Size = PrintFontSize
    Printer.Font.Bold = PrintFontBold
    Printer.Font.Underline = PrintFontUnderline
    Printer.Font.StrikeThrough = PrintFontStrikethrough
    
    
    While InStr(NewString, "^") > 0
        UpPosition = InStr(NewString, "^")
        NewStringLength = Len(NewString)
        UpSpacePosition = InStr(UpPosition, NewString, " ")
        If UpSpacePosition = 0 Then UpSpacePosition = NewStringLength
        COuntUp = COuntUp + 1
        PrintString = Left(NewString, UpSpacePosition)
        PrintedString = PrintedString + PrintString
        NewString = Right(NewString, NewStringLength - UpPosition - 1)
        
        MyX1 = MyX1 + Printer.TextWidth(PrintedString)
        
        SuperScriptPrint PrintString, MyX1, X2, Y1, Y2, PrintFontName, PrintFontSize, PrintFontBold, PrintFontItalic, PrintFontUnderline, PrintFontStrikethrough, TextAlignment.LeftAlign
        PrintString = ""
'        SuperScriptPrint SuppliedText, X1, X2, Y1, Y2, PrintFontName, PrintFontSize, PrintFontBold, PrintFontItalic, PrintFontUnderline, PrintFontStrikethrough, PrintAlignment
    Wend
    
    SuperScriptPrint PrintString, MyX1, X2, Y1, Y2, PrintFontName, PrintFontSize, PrintFontBold, PrintFontItalic, PrintFontUnderline, PrintFontStrikethrough, PrintAlignment
    
End Sub
    
    
Private Sub SuperScriptPrint(ByVal SuppliedText As String, ByVal X1 As Long, ByVal X2 As Long, ByVal Y1 As Long, ByVal Y2 As Long, PrintFontName As String, PrintFontSize As Long, PrintFontBold As Boolean, PrintFontItalic As Boolean, PrintFontUnderline As Boolean, PrintFontStrikethrough As Boolean, PrintAlignment As Integer)
    Dim SuperscriptPresent As Boolean
    Dim SuperscriptLocation As Long
    Dim BeforeSuperscriptText As String
    Dim AfterSuperscriptText As String
    Dim SuperScriptText As String
    Dim SpaceLocation As Long
    Dim SuperscriptElevation As Double
    
    Dim TotalTextWidth As Long
    Dim TextWidth1 As Long
    Dim TextWidth2 As Long
    Dim TextWidth3 As Long
    
    SuperscriptLocation = InStr(1, SuppliedText, "^")
    SuperscriptElevation = 0.2
    
    If SuperscriptLocation = 0 Then
        Printer.Font.Name = PrintFontName
        Printer.Font.Size = PrintFontSize
        Printer.Font.Bold = PrintFontBold
        Printer.Font.Underline = PrintFontUnderline
        Printer.Font.StrikeThrough = PrintFontStrikethrough
        Printer.CurrentY = Y1
        If PrintAlignment = TextAlignment.LeftAlign Then
            Printer.CurrentX = X1
        ElseIf PrintAlignment = TextAlignment.RightAlign Then
            Printer.CurrentX = X2 - Printer.TextWidth(SuppliedText)
        ElseIf PrintAlignment = TextAlignment.CenterAlign Then
            Printer.CurrentX = ((X1 + X2) / 2) - (Printer.TextWidth(SuppliedText) / 2)
        Else
            Printer.CurrentX = ((X1 + X2) / 2) - (Printer.TextWidth(SuppliedText) / 2)
        End If
        Printer.Print SuppliedText
    Else
        If SuperscriptLocation > 0 Then BeforeSuperscriptText = Left(SuppliedText, SuperscriptLocation - 1)
        
        AfterSuperscriptText = Right(SuppliedText, (Len(SuppliedText) - SuperscriptLocation))
        
        If InStr(1, AfterSuperscriptText, " ") = 0 Then
            
            AfterSuperscriptText = Trim(AfterSuperscriptText)
            
            Printer.Font.Name = PrintFontName
            Printer.Font.Size = PrintFontSize
            Printer.Font.Bold = PrintFontBold
            Printer.Font.Underline = PrintFontUnderline
            Printer.Font.StrikeThrough = PrintFontStrikethrough
            
            TextWidth1 = Printer.TextWidth(BeforeSuperscriptText)
            
            Printer.FontSize = PrintFontSize - 2
            
            TextWidth2 = Printer.TextWidth(AfterSuperscriptText)
            
            TextWidth3 = 0
            
            TotalTextWidth = TextWidth1 + TextWidth2 + TextWidth3
            
            Printer.Font.Name = PrintFontName
            Printer.Font.Size = PrintFontSize
            Printer.Font.Bold = PrintFontBold
            Printer.Font.Underline = PrintFontUnderline
            Printer.Font.StrikeThrough = PrintFontStrikethrough
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                Printer.CurrentX = X1
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                Printer.CurrentX = X2 - TotalTextWidth
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2)
            Else
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2)
            End If
            
            Printer.CurrentY = Y1
            Printer.Print BeforeSuperscriptText
            
            
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                Printer.CurrentX = X1 + TextWidth1
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                Printer.CurrentX = X2 - TotalTextWidth + TextWidth1
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1
            Else
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1
            End If
            Printer.CurrentY = Y1 - (Printer.TextHeight(BeforeSuperscriptText) * SuperscriptElevation)
            Printer.FontSize = PrintFontSize - 2
            
            Printer.Print AfterSuperscriptText
            
            Printer.FontSize = PrintFontSize
            
        Else
            SpaceLocation = InStr(1, AfterSuperscriptText, " ")
            SuperScriptText = Left(AfterSuperscriptText, SpaceLocation)
            AfterSuperscriptText = Right(AfterSuperscriptText, (Len(AfterSuperscriptText) - SpaceLocation))
            Printer.Font.Name = PrintFontName
            Printer.Font.Size = PrintFontSize
            Printer.Font.Bold = PrintFontBold
            Printer.Font.Underline = PrintFontUnderline
            Printer.Font.StrikeThrough = PrintFontStrikethrough
            
            TextWidth1 = Printer.TextWidth(BeforeSuperscriptText)
            Printer.FontSize = PrintFontSize - 2
            TextWidth2 = Printer.TextWidth(SuperScriptText)
            Printer.FontSize = PrintFontSize
            TextWidth3 = Printer.TextWidth(AfterSuperscriptText)
            TotalTextWidth = TextWidth1 + TextWidth2 + TextWidth3
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                Printer.CurrentX = X1
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                Printer.CurrentX = X2 - TotalTextWidth
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2)
            Else
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2)
            End If
            
            Printer.CurrentY = Y1
            Printer.Print BeforeSuperscriptText
            
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                Printer.CurrentX = X1 + TextWidth1
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                Printer.CurrentX = X2 - TotalTextWidth + TextWidth1
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1
            Else
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1
            End If
            
            
            Printer.CurrentY = Y1 - (Printer.TextHeight(BeforeSuperscriptText) * SuperscriptElevation)
            Printer.FontSize = PrintFontSize - 2
            Printer.Print SuperScriptText
            
            
            
            Printer.FontSize = PrintFontSize
            Printer.CurrentY = Y1
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                Printer.CurrentX = X1 + TextWidth1 + TextWidth2
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                Printer.CurrentX = X2 - TotalTextWidth + TextWidth1 + TextWidth2
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1 + TextWidth2
            Else
                Printer.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1 + TextWidth2
            End If
            
            Printer.Print AfterSuperscriptText
        End If
    End If
End Sub
