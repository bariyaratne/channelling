VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Agent"
Attribute VB_Ext_KEY = "Member1" ,"Doctor"
Attribute VB_Ext_KEY = "Member2" ,"Patient"
Option Explicit
    Dim temSQL As String
    Private varPatientFacility_ID As Long
    Private varUser_ID As Long
    Private varRepayUser_ID As Long
    Private varCreditSettleUser_ID As Long
    Private varPatientID As Long
    Private varHospitalFacility_ID As Long
    Private varFacilityStaff_ID As Long
    Private varFacilityCatogery
    Private varStaff_ID As Long
    Private varPatientBill_ID As Long
    Private varBookingDate As Date
    Private varBookingTime As Date
    Private varAppointmentDate As Date
    Private varSecession As Long
    Private varAppointmentTime As Date
    Private varDaySerial As Long
    Private varPersonalFee As Double
    Private varPersonalFeeToPay As Double
    Private varInstitutionFee As Double
    Private varInstitutionFeeToPay As Double
    Private varOtherFee As Double
    Private varOtherFeeToPay As Double
    Private varTotalFee As Double
    Private varTotalFeeToPay As Double
    Private varFullyPaid As Boolean
    Private varFullyPaidNull As Long
    Private varResultSuccess As Boolean
    Private varPersonalRefund As Double
    Private varPersonalRefundComment As String
    Private varInstitutionRefund As Double
    Private varInstitutionRefundComment As String
    Private varOtherRefund As Double
    Private varOtherRefundComment As String
    Private varTotalRefund As Double
    Private varRefundToPatient As Boolean
    Private varRefundToAgent As Boolean
    Private varRepayComments As String
    Private varNewBooking As Boolean
    Private varCarriedForwardID As Long
    Private varBoughtForwardID As Long
    Private varPaidToSTaff As Boolean
    Private varStaffPayment As Double
    Private varPaidToStaffOn As Date
    Private varPaidToStaffUser As Long
    Private varStaffPayment_ID As Long
    Private varCancelled As Boolean
    Private varCancelledNull As Long
    Private varCancelRemark As String
    Private varRefund As Boolean
    Private varRefundNull As Long
    Private varRefundRemark As String
    Private varPaymentMode As String
    Private varAgent_ID As Long
    Private varCreditAgent_ID As Long
    Private varPaymentMethod_Id As Long
    Private varBillPrinted As Boolean
    Private varRepayDate As Date
    Private varRepayTime As Date
    Private varRepayComment As String
    Private varSettleCashDate As Date
    Private varSettleCashTime As Date
    Private varC As Boolean
    Private varIsADoctor As Boolean
    Private varIsAStaffMember As Boolean
    Private varIsAnInvestigation As Boolean
    Private varAgentRefNo As String
    Private varPatientAbsent As Boolean
    Private varPatientAbsentNull As Long
    Private varPersonalDue As Double
    Private varInstitutionDue As Double
    Private varOtherDue As Double
    Private varTotalDue As Double
    Private varAgentValidateStaffID As Long
    Private varAgentValidateDate As Date
    Private varAgentValidateTime As Date
    Private varIsAForeigner As Boolean
        
    Private mvarCreditSettleUser As staff
    Private mvarRepayUser As staff
    Private mvarFacilityStaff As staff
    Private mvarUser As staff
    Private mvarDoctor As Doctor
    Private mvarPatient As Patient 'local copy
    Private mvarAgent As Agent 'local copy

Public Property Set Agent(ByVal vData As Agent)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Agent = Form1
    Set mvarAgent = vData
End Property


Public Property Get Agent() As Agent
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Agent
    Set Agent = mvarAgent
End Property



    
Public Property Set Patient(ByVal vData As Patient)
    Set mvarPatient = vData
End Property


Public Property Get Patient() As Patient
    Set Patient = mvarPatient
End Property




Public Property Set Doctor(ByVal vData As Doctor)
    Set mvarDoctor = vData
    varStaff_ID = vData.Doctor_ID
End Property

Public Property Get Doctor() As Doctor
    Set Doctor = mvarDoctor
End Property

Public Property Set User(ByVal vData As staff)
    Set mvarUser = vData
    varUser_ID = vData.Staff_ID
End Property

Public Property Get User() As staff
    Set User = mvarUser
End Property

Public Property Set FacilityStaff(ByVal vData As staff)
    Set mvarFacilityStaff = vData
    varFacilityStaff_ID = vData.Staff_ID
End Property

Public Property Get FacilityStaff() As staff
    Set FacilityStaff = mvarFacilityStaff
End Property

Public Property Set RepayUser(ByVal vData As staff)
    Set mvarRepayUser = vData
    varRepayUser_ID = vData.Staff_ID
End Property


Public Property Get RepayUser() As staff
    Set RepayUser = mvarRepayUser
End Property

Public Property Set CreditSettleUser(ByVal vData As staff)
    Set mvarCreditSettleUser = vData
    varCreditSettleUser_ID = vData.Staff_ID
End Property


Public Property Get CreditSettleUser() As staff
    Set CreditSettleUser = mvarCreditSettleUser
End Property

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientFacility"
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If varPatientFacility_ID = 0 Then .AddNew
        !User_ID = varUser_ID
        !RepayUser_ID = varRepayUser_ID
        !CreditSettleUser_ID = varCreditSettleUser_ID
        !patientid = varPatientID
        !HospitalFacility_ID = varHospitalFacility_ID
        !FacilityStaff_ID = varFacilityStaff_ID
        !FacilityCatogery = varFacilityCatogery
        !Staff_ID = varStaff_ID
        !PatientBill_ID = varPatientBill_ID
        !BookingDate = varBookingDate
        !BookingTime = varBookingTime
        !AppointmentDate = varAppointmentDate
        !Secession = varSecession
        !AppointmentTime = varAppointmentTime
        !DaySerial = varDaySerial
        !PersonalFee = varPersonalFee
        !PersonalFeeToPay = varPersonalFeeToPay
        !InstitutionFee = varInstitutionFee
        !InstitutionFeeToPay = varInstitutionFeeToPay
        !otherFee = varOtherFee
        !OtherFeeToPay = varOtherFeeToPay
        !totalFee = varTotalFee
        !TotalFeeToPay = varTotalFeeToPay
        !FullyPaid = varFullyPaid
        !FullyPaidNull = varFullyPaidNull
        !ResultSuccess = varResultSuccess
        !PersonalRefund = varPersonalRefund
        !PersonalRefundComment = varPersonalRefundComment
        !InstitutionRefund = varInstitutionRefund
        !InstitutionRefundComment = varInstitutionRefundComment
        !OtherRefund = varOtherRefund
        !OtherRefundComment = varOtherRefundComment
        !TotalRefund = varTotalRefund
        !RefundToPatient = varRefundToPatient
        !RefundToAgent = varRefundToAgent
        !RepayComments = varRepayComments
        !NewBooking = varNewBooking
        !CarriedForwardID = varCarriedForwardID
        !BoughtForwardID = varBoughtForwardID
        !PaidToSTaff = varPaidToSTaff
        !StaffPayment = varStaffPayment
        !PaidToStaffOn = varPaidToStaffOn
        !PaidToStaffUser = varPaidToStaffUser
        !StaffPayment_ID = varStaffPayment_ID
        !Cancelled = varCancelled
        !CancelledNull = varCancelledNull
        !CancelRemark = varCancelRemark
        !Refund = varRefund
        !RefundNull = varRefundNull
        !RefundRemark = varRefundRemark
        !PaymentMode = varPaymentMode
        !Agent_ID = varAgent_ID
        !CreditAgent_ID = varCreditAgent_ID
        !PaymentMethod_Id = varPaymentMethod_Id
        !BillPrinted = varBillPrinted
        !RepayDate = varRepayDate
        !RepayTime = varRepayTime
        !RepayComment = varRepayComment
        !SettleCashDate = varSettleCashDate
        !SettleCashTime = varSettleCashTime
        !C = varC
        !IsADoctor = varIsADoctor
        !IsAStaffMember = varIsAStaffMember
        !IsAnInvestigation = varIsAnInvestigation
        !AgentRefNo = varAgentRefNo
        !PatientAbsent = varPatientAbsent
        !PatientAbsentNull = varPatientAbsentNull
        !PersonalDue = varPersonalDue
        !InstitutionDue = varInstitutionDue
        !OtherDue = varOtherDue
        !TotalDue = varTotalDue
        !AgentValidateStaffID = varAgentValidateStaffID
        !AgentValidateDate = varAgentValidateDate
        !AgentValidateTime = varAgentValidateTime
        !IsAForeigner = varIsAForeigner
        .Update
        varPatientFacility_ID = !PatientFacility_ID
        If .State = 1 Then .Close
    End With
    
    
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientFacility WHERE PatientFacility_ID = " & varPatientFacility_ID
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If Not IsNull(!PatientFacility_ID) Then
           varPatientFacility_ID = !PatientFacility_ID
        End If
        If Not IsNull(!User_ID) Then
           varUser_ID = !User_ID
        End If
        If Not IsNull(!RepayUser_ID) Then
           varRepayUser_ID = !RepayUser_ID
        End If
        If Not IsNull(!CreditSettleUser_ID) Then
           varCreditSettleUser_ID = !CreditSettleUser_ID
        End If
        If Not IsNull(!patientid) Then
           varPatientID = !patientid
        End If
        If Not IsNull(!HospitalFacility_ID) Then
           varHospitalFacility_ID = !HospitalFacility_ID
        End If
        If Not IsNull(!FacilityStaff_ID) Then
           varFacilityStaff_ID = !FacilityStaff_ID
        End If
        If Not IsNull(!FacilityCatogery) Then
           varFacilityCatogery = !FacilityCatogery
        End If
        If Not IsNull(!Staff_ID) Then
           varStaff_ID = !Staff_ID
        End If
        If Not IsNull(!PatientBill_ID) Then
           varPatientBill_ID = !PatientBill_ID
        End If
        If Not IsNull(!BookingDate) Then
           varBookingDate = !BookingDate
        End If
        If Not IsNull(!BookingTime) Then
           varBookingTime = !BookingTime
        End If
        If Not IsNull(!AppointmentDate) Then
           varAppointmentDate = !AppointmentDate
        End If
        If Not IsNull(!Secession) Then
           varSecession = !Secession
        End If
        If Not IsNull(!AppointmentTime) Then
           varAppointmentTime = !AppointmentTime
        End If
        If Not IsNull(!DaySerial) Then
           varDaySerial = !DaySerial
        End If
        If Not IsNull(!PersonalFee) Then
           varPersonalFee = !PersonalFee
        End If
        If Not IsNull(!PersonalFeeToPay) Then
           varPersonalFeeToPay = !PersonalFeeToPay
        End If
        If Not IsNull(!InstitutionFee) Then
           varInstitutionFee = !InstitutionFee
        End If
        If Not IsNull(!InstitutionFeeToPay) Then
           varInstitutionFeeToPay = !InstitutionFeeToPay
        End If
        If Not IsNull(!otherFee) Then
           varOtherFee = !otherFee
        End If
        If Not IsNull(!OtherFeeToPay) Then
           varOtherFeeToPay = !OtherFeeToPay
        End If
        If Not IsNull(!totalFee) Then
           varTotalFee = !totalFee
        End If
        If Not IsNull(!TotalFeeToPay) Then
           varTotalFeeToPay = !TotalFeeToPay
        End If
        If Not IsNull(!FullyPaid) Then
           varFullyPaid = !FullyPaid
        End If
        If Not IsNull(!FullyPaidNull) Then
           varFullyPaidNull = !FullyPaidNull
        End If
        If Not IsNull(!ResultSuccess) Then
           varResultSuccess = !ResultSuccess
        End If
        If Not IsNull(!PersonalRefund) Then
           varPersonalRefund = !PersonalRefund
        End If
        If Not IsNull(!PersonalRefundComment) Then
           varPersonalRefundComment = !PersonalRefundComment
        End If
        If Not IsNull(!InstitutionRefund) Then
           varInstitutionRefund = !InstitutionRefund
        End If
        If Not IsNull(!InstitutionRefundComment) Then
           varInstitutionRefundComment = !InstitutionRefundComment
        End If
        If Not IsNull(!OtherRefund) Then
           varOtherRefund = !OtherRefund
        End If
        If Not IsNull(!OtherRefundComment) Then
           varOtherRefundComment = !OtherRefundComment
        End If
        If Not IsNull(!TotalRefund) Then
           varTotalRefund = !TotalRefund
        End If
        If Not IsNull(!RefundToPatient) Then
           varRefundToPatient = !RefundToPatient
        End If
        If Not IsNull(!RefundToAgent) Then
           varRefundToAgent = !RefundToAgent
        End If
        If Not IsNull(!RepayComments) Then
           varRepayComments = !RepayComments
        End If
        If Not IsNull(!NewBooking) Then
           varNewBooking = !NewBooking
        End If
        If Not IsNull(!CarriedForwardID) Then
           varCarriedForwardID = !CarriedForwardID
        End If
        If Not IsNull(!BoughtForwardID) Then
           varBoughtForwardID = !BoughtForwardID
        End If
        If Not IsNull(!PaidToSTaff) Then
           varPaidToSTaff = !PaidToSTaff
        End If
        If Not IsNull(!StaffPayment) Then
           varStaffPayment = !StaffPayment
        End If
        If Not IsNull(!PaidToStaffOn) Then
           varPaidToStaffOn = !PaidToStaffOn
        End If
        If Not IsNull(!PaidToStaffUser) Then
           varPaidToStaffUser = !PaidToStaffUser
        End If
        If Not IsNull(!StaffPayment_ID) Then
           varStaffPayment_ID = !StaffPayment_ID
        End If
        If Not IsNull(!Cancelled) Then
           varCancelled = !Cancelled
        End If
        If Not IsNull(!CancelledNull) Then
           varCancelledNull = !CancelledNull
        End If
        If Not IsNull(!CancelRemark) Then
           varCancelRemark = !CancelRemark
        End If
        If Not IsNull(!Refund) Then
           varRefund = !Refund
        End If
        If Not IsNull(!RefundNull) Then
           varRefundNull = !RefundNull
        End If
        If Not IsNull(!RefundRemark) Then
           varRefundRemark = !RefundRemark
        End If
        If Not IsNull(!PaymentMode) Then
           varPaymentMode = !PaymentMode
        End If
        If Not IsNull(!Agent_ID) Then
           varAgent_ID = !Agent_ID
        End If
        If Not IsNull(!CreditAgent_ID) Then
           varCreditAgent_ID = !CreditAgent_ID
        End If
        If Not IsNull(!PaymentMethod_Id) Then
           varPaymentMethod_Id = !PaymentMethod_Id
        End If
        If Not IsNull(!BillPrinted) Then
           varBillPrinted = !BillPrinted
        End If
        If Not IsNull(!RepayDate) Then
           varRepayDate = !RepayDate
        End If
        If Not IsNull(!RepayTime) Then
           varRepayTime = !RepayTime
        End If
        If Not IsNull(!RepayComment) Then
           varRepayComment = !RepayComment
        End If
        If Not IsNull(!SettleCashDate) Then
           varSettleCashDate = !SettleCashDate
        End If
        If Not IsNull(!SettleCashTime) Then
           varSettleCashTime = !SettleCashTime
        End If
        If Not IsNull(!C) Then
           varC = !C
        End If
        If Not IsNull(!IsADoctor) Then
           varIsADoctor = !IsADoctor
        End If
        If Not IsNull(!IsAStaffMember) Then
           varIsAStaffMember = !IsAStaffMember
        End If
        If Not IsNull(!IsAnInvestigation) Then
           varIsAnInvestigation = !IsAnInvestigation
        End If
        If Not IsNull(!AgentRefNo) Then
           varAgentRefNo = !AgentRefNo
        End If
        If Not IsNull(!PatientAbsent) Then
           varPatientAbsent = !PatientAbsent
        End If
        If Not IsNull(!PatientAbsentNull) Then
           varPatientAbsentNull = !PatientAbsentNull
        End If
        If Not IsNull(!PersonalDue) Then
           varPersonalDue = !PersonalDue
        End If
        If Not IsNull(!InstitutionDue) Then
           varInstitutionDue = !InstitutionDue
        End If
        If Not IsNull(!OtherDue) Then
           varOtherDue = !OtherDue
        End If
        If Not IsNull(!TotalDue) Then
           varTotalDue = !TotalDue
        End If
        If Not IsNull(!AgentValidateStaffID) Then
           varAgentValidateStaffID = !AgentValidateStaffID
        End If
        If Not IsNull(!AgentValidateDate) Then
           varAgentValidateDate = !AgentValidateDate
        End If
        If Not IsNull(!AgentValidateTime) Then
           varAgentValidateTime = !AgentValidateTime
        End If
        If Not IsNull(!IsAForeigner) Then
           varIsAForeigner = !IsAForeigner
        End If
    If .State = 1 Then .Close
    End With
    

'    Private mvarCreditSettleUser As Staff
'    Private mvarRepayUser As Staff
'    Private mvarFacilityStaff As Staff
'    Private mvarUser As Staff
'    Private mvarDoctor As Doctor
    
    Set mvarCreditSettleUser = New staff
    mvarCreditSettleUser.Staff_ID = varCreditSettleUser_ID
    
    Set mvarRepayUser = New staff
    mvarRepayUser.Staff_ID = varRepayUser_ID
    
    Set mvarFacilityStaff = New staff
    mvarFacilityStaff.Staff_ID = varFacilityStaff_ID
    
    Set mvarUser = New staff
    mvarUser.Staff_ID = varUser_ID
    
    Set mvarDoctor = New Doctor
    mvarDoctor.Doctor_ID = varStaff_ID
    
    Set mvarPatient = New Patient
    mvarPatient.Patient_ID = varPatientID
    
    Set mvarAgent = New Agent
    mvarAgent.Institution_ID = varAgent_ID
End Sub
Public Sub clearData()
    varPatientFacility_ID = 0
    varUser_ID = 0
    varRepayUser_ID = 0
    varCreditSettleUser_ID = 0
    varPatientID = 0
    varHospitalFacility_ID = 0
    varFacilityStaff_ID = 0
    varFacilityCatogery = Empty
    varStaff_ID = 0
    varPatientBill_ID = 0
    varBookingDate = Empty
    varBookingTime = Empty
    varAppointmentDate = Empty
    varSecession = 0
    varAppointmentTime = Empty
    varDaySerial = 0
    varPersonalFee = 0
    varPersonalFeeToPay = 0
    varInstitutionFee = 0
    varInstitutionFeeToPay = 0
    varOtherFee = 0
    varOtherFeeToPay = 0
    varTotalFee = 0
    varTotalFeeToPay = 0
    varFullyPaid = False
    varFullyPaidNull = 0
    varResultSuccess = False
    varPersonalRefund = 0
    varPersonalRefundComment = Empty
    varInstitutionRefund = 0
    varInstitutionRefundComment = Empty
    varOtherRefund = 0
    varOtherRefundComment = Empty
    varTotalRefund = 0
    varRefundToPatient = False
    varRefundToAgent = False
    varRepayComments = Empty
    varNewBooking = False
    varCarriedForwardID = 0
    varBoughtForwardID = 0
    varPaidToSTaff = False
    varStaffPayment = 0
    varPaidToStaffOn = Empty
    varPaidToStaffUser = 0
    varStaffPayment_ID = 0
    varCancelled = False
    varCancelledNull = 0
    varCancelRemark = Empty
    varRefund = False
    varRefundNull = 0
    varRefundRemark = Empty
    varPaymentMode = Empty
    varAgent_ID = 0
    varCreditAgent_ID = 0
    varPaymentMethod_Id = 0
    varBillPrinted = False
    varRepayDate = Empty
    varRepayTime = Empty
    varRepayComment = Empty
    varSettleCashDate = Empty
    varSettleCashTime = Empty
    varC = False
    varIsADoctor = False
    varIsAStaffMember = False
    varIsAnInvestigation = False
    varAgentRefNo = Empty
    varPatientAbsent = False
    varPatientAbsentNull = 0
    varPersonalDue = 0
    varInstitutionDue = 0
    varOtherDue = 0
    varTotalDue = 0
    varAgentValidateStaffID = 0
    varAgentValidateDate = Empty
    varAgentValidateTime = Empty
    
    Set mvarCreditSettleUser = Nothing
    Set mvarRepayUser = Nothing
    Set mvarFacilityStaff = Nothing
    Set mvarUser = Nothing
    Set mvarDoctor = Nothing
    
End Sub

Public Property Let PatientFacility_ID(ByVal vPatientFacility_ID As Long)
    Call clearData
    varPatientFacility_ID = vPatientFacility_ID
    Call loadData
End Property

Public Property Get PatientFacility_ID() As Long
    PatientFacility_ID = varPatientFacility_ID
End Property

Public Property Let User_ID(ByVal vUser_ID As Long)
    varUser_ID = vUser_ID
End Property

Public Property Get User_ID() As Long
    User_ID = varUser_ID
End Property

Public Property Let RepayUser_ID(ByVal vRepayUser_ID As Long)
    varRepayUser_ID = vRepayUser_ID
End Property

Public Property Get RepayUser_ID() As Long
    RepayUser_ID = varRepayUser_ID
End Property

Public Property Let CreditSettleUser_ID(ByVal vCreditSettleUser_ID As Long)
    varCreditSettleUser_ID = vCreditSettleUser_ID
End Property

Public Property Get CreditSettleUser_ID() As Long
    CreditSettleUser_ID = varCreditSettleUser_ID
End Property

Public Property Let patientid(ByVal vPatientID As Long)
    varPatientID = vPatientID
End Property

Public Property Get patientid() As Long
    patientid = varPatientID
End Property

Public Property Let HospitalFacility_ID(ByVal vHospitalFacility_ID As Long)
    varHospitalFacility_ID = vHospitalFacility_ID
End Property

Public Property Get HospitalFacility_ID() As Long
    HospitalFacility_ID = varHospitalFacility_ID
End Property

Public Property Let FacilityStaff_ID(ByVal vFacilityStaff_ID As Long)
    varFacilityStaff_ID = vFacilityStaff_ID
End Property

Public Property Get FacilityStaff_ID() As Long
    FacilityStaff_ID = varFacilityStaff_ID
End Property

Public Property Let FacilityCatogery(ByVal vFacilityCatogery)
    varFacilityCatogery = vFacilityCatogery
End Property

Public Property Get FacilityCatogery()
    FacilityCatogery = varFacilityCatogery
End Property

Public Property Let Staff_ID(ByVal vStaff_ID As Long)
    varStaff_ID = vStaff_ID
End Property

Public Property Get Staff_ID() As Long
    Staff_ID = varStaff_ID
End Property

Public Property Let PatientBill_ID(ByVal vPatientBill_ID As Long)
    varPatientBill_ID = vPatientBill_ID
End Property

Public Property Get PatientBill_ID() As Long
    PatientBill_ID = varPatientBill_ID
End Property

Public Property Let BookingDate(ByVal vBookingDate As Date)
    varBookingDate = vBookingDate
End Property

Public Property Get BookingDate() As Date
    BookingDate = varBookingDate
End Property

Public Property Let BookingTime(ByVal vBookingTime As Date)
    varBookingTime = vBookingTime
End Property

Public Property Get BookingTime() As Date
    BookingTime = varBookingTime
End Property

Public Property Let AppointmentDate(ByVal vAppointmentDate As Date)
    varAppointmentDate = vAppointmentDate
End Property

Public Property Get AppointmentDate() As Date
    AppointmentDate = varAppointmentDate
End Property

Public Property Let Secession(ByVal vSecession As Long)
    varSecession = vSecession
End Property

Public Property Get Secession() As Long
    Secession = varSecession
End Property

Public Property Let AppointmentTime(ByVal vAppointmentTime As Date)
    varAppointmentTime = vAppointmentTime
End Property

Public Property Get AppointmentTime() As Date
    AppointmentTime = varAppointmentTime
End Property

Public Property Let DaySerial(ByVal vDaySerial As Long)
    varDaySerial = vDaySerial
End Property

Public Property Get DaySerial() As Long
    DaySerial = varDaySerial
End Property

Public Property Let PersonalFee(ByVal vPersonalFee As Double)
    varPersonalFee = vPersonalFee
End Property

Public Property Get PersonalFee() As Double
    PersonalFee = varPersonalFee
End Property

Public Property Let PersonalFeeToPay(ByVal vPersonalFeeToPay As Double)
    varPersonalFeeToPay = vPersonalFeeToPay
End Property

Public Property Get PersonalFeeToPay() As Double
    PersonalFeeToPay = varPersonalFeeToPay
End Property

Public Property Let InstitutionFee(ByVal vInstitutionFee As Double)
    varInstitutionFee = vInstitutionFee
End Property

Public Property Get InstitutionFee() As Double
    InstitutionFee = varInstitutionFee
End Property

Public Property Let InstitutionFeeToPay(ByVal vInstitutionFeeToPay As Double)
    varInstitutionFeeToPay = vInstitutionFeeToPay
End Property

Public Property Get InstitutionFeeToPay() As Double
    InstitutionFeeToPay = varInstitutionFeeToPay
End Property

Public Property Let otherFee(ByVal vOtherFee As Double)
    varOtherFee = vOtherFee
End Property

Public Property Get otherFee() As Double
    otherFee = varOtherFee
End Property

Public Property Let OtherFeeToPay(ByVal vOtherFeeToPay As Double)
    varOtherFeeToPay = vOtherFeeToPay
End Property

Public Property Get OtherFeeToPay() As Double
    OtherFeeToPay = varOtherFeeToPay
End Property

Public Property Let totalFee(ByVal vTotalFee As Double)
    varTotalFee = vTotalFee
End Property

Public Property Get totalFee() As Double
    totalFee = varTotalFee
End Property

Public Property Let TotalFeeToPay(ByVal vTotalFeeToPay As Double)
    varTotalFeeToPay = vTotalFeeToPay
End Property

Public Property Get TotalFeeToPay() As Double
    TotalFeeToPay = varTotalFeeToPay
End Property

Public Property Let FullyPaid(ByVal vFullyPaid As Boolean)
    varFullyPaid = vFullyPaid
End Property

Public Property Get FullyPaid() As Boolean
    FullyPaid = varFullyPaid
End Property

Public Property Let FullyPaidNull(ByVal vFullyPaidNull As Long)
    varFullyPaidNull = vFullyPaidNull
End Property

Public Property Get FullyPaidNull() As Long
    FullyPaidNull = varFullyPaidNull
End Property

Public Property Let ResultSuccess(ByVal vResultSuccess As Boolean)
    varResultSuccess = vResultSuccess
End Property

Public Property Get ResultSuccess() As Boolean
    ResultSuccess = varResultSuccess
End Property

Public Property Let PersonalRefund(ByVal vPersonalRefund As Double)
    varPersonalRefund = vPersonalRefund
End Property

Public Property Get PersonalRefund() As Double
    PersonalRefund = varPersonalRefund
End Property

Public Property Let PersonalRefundComment(ByVal vPersonalRefundComment As String)
    varPersonalRefundComment = vPersonalRefundComment
End Property

Public Property Get PersonalRefundComment() As String
    PersonalRefundComment = varPersonalRefundComment
End Property

Public Property Let InstitutionRefund(ByVal vInstitutionRefund As Double)
    varInstitutionRefund = vInstitutionRefund
End Property

Public Property Get InstitutionRefund() As Double
    InstitutionRefund = varInstitutionRefund
End Property

Public Property Let InstitutionRefundComment(ByVal vInstitutionRefundComment As String)
    varInstitutionRefundComment = vInstitutionRefundComment
End Property

Public Property Get InstitutionRefundComment() As String
    InstitutionRefundComment = varInstitutionRefundComment
End Property

Public Property Let OtherRefund(ByVal vOtherRefund As Double)
    varOtherRefund = vOtherRefund
End Property

Public Property Get OtherRefund() As Double
    OtherRefund = varOtherRefund
End Property

Public Property Let OtherRefundComment(ByVal vOtherRefundComment As String)
    varOtherRefundComment = vOtherRefundComment
End Property

Public Property Get OtherRefundComment() As String
    OtherRefundComment = varOtherRefundComment
End Property

Public Property Let TotalRefund(ByVal vTotalRefund As Double)
    varTotalRefund = vTotalRefund
End Property

Public Property Get TotalRefund() As Double
    TotalRefund = varTotalRefund
End Property

Public Property Let RefundToPatient(ByVal vRefundToPatient As Boolean)
    varRefundToPatient = vRefundToPatient
End Property

Public Property Get RefundToPatient() As Boolean
    RefundToPatient = varRefundToPatient
End Property

Public Property Let RefundToAgent(ByVal vRefundToAgent As Boolean)
    varRefundToAgent = vRefundToAgent
End Property

Public Property Get RefundToAgent() As Boolean
    RefundToAgent = varRefundToAgent
End Property

Public Property Let RepayComments(ByVal vRepayComments As String)
    varRepayComments = vRepayComments
End Property

Public Property Get RepayComments() As String
    RepayComments = varRepayComments
End Property

Public Property Let NewBooking(ByVal vNewBooking As Boolean)
    varNewBooking = vNewBooking
End Property

Public Property Get NewBooking() As Boolean
    NewBooking = varNewBooking
End Property

Public Property Let CarriedForwardID(ByVal vCarriedForwardID As Long)
    varCarriedForwardID = vCarriedForwardID
End Property

Public Property Get CarriedForwardID() As Long
    CarriedForwardID = varCarriedForwardID
End Property

Public Property Let BoughtForwardID(ByVal vBoughtForwardID As Long)
    varBoughtForwardID = vBoughtForwardID
End Property

Public Property Get BoughtForwardID() As Long
    BoughtForwardID = varBoughtForwardID
End Property

Public Property Let PaidToSTaff(ByVal vPaidToSTaff As Boolean)
    varPaidToSTaff = vPaidToSTaff
End Property

Public Property Get PaidToSTaff() As Boolean
    PaidToSTaff = varPaidToSTaff
End Property

Public Property Let StaffPayment(ByVal vStaffPayment As Double)
    varStaffPayment = vStaffPayment
End Property

Public Property Get StaffPayment() As Double
    StaffPayment = varStaffPayment
End Property

Public Property Let PaidToStaffOn(ByVal vPaidToStaffOn As Date)
    varPaidToStaffOn = vPaidToStaffOn
End Property

Public Property Get PaidToStaffOn() As Date
    PaidToStaffOn = varPaidToStaffOn
End Property

Public Property Let PaidToStaffUser(ByVal vPaidToStaffUser As Long)
    varPaidToStaffUser = vPaidToStaffUser
End Property

Public Property Get PaidToStaffUser() As Long
    PaidToStaffUser = varPaidToStaffUser
End Property

Public Property Let StaffPayment_ID(ByVal vStaffPayment_ID As Long)
    varStaffPayment_ID = vStaffPayment_ID
End Property

Public Property Get StaffPayment_ID() As Long
    StaffPayment_ID = varStaffPayment_ID
End Property

Public Property Let Cancelled(ByVal vCancelled As Boolean)
    varCancelled = vCancelled
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = varCancelled
End Property

Public Property Let CancelledNull(ByVal vCancelledNull As Long)
    varCancelledNull = vCancelledNull
End Property

Public Property Get CancelledNull() As Long
    CancelledNull = varCancelledNull
End Property

Public Property Let CancelRemark(ByVal vCancelRemark As String)
    varCancelRemark = vCancelRemark
End Property

Public Property Get CancelRemark() As String
    CancelRemark = varCancelRemark
End Property

Public Property Let Refund(ByVal vRefund As Boolean)
    varRefund = vRefund
End Property

Public Property Get Refund() As Boolean
    Refund = varRefund
End Property

Public Property Let RefundNull(ByVal vRefundNull As Long)
    varRefundNull = vRefundNull
End Property

Public Property Get RefundNull() As Long
    RefundNull = varRefundNull
End Property

Public Property Let RefundRemark(ByVal vRefundRemark As String)
    varRefundRemark = vRefundRemark
End Property

Public Property Get RefundRemark() As String
    RefundRemark = varRefundRemark
End Property

Public Property Let PaymentMode(ByVal vPaymentMode As String)
    varPaymentMode = vPaymentMode
End Property

Public Property Get PaymentMode() As String
    PaymentMode = varPaymentMode
End Property

Public Property Let Agent_ID(ByVal vAgent_ID As Long)
    varAgent_ID = vAgent_ID
End Property

Public Property Get Agent_ID() As Long
    Agent_ID = varAgent_ID
End Property

Public Property Let CreditAgent_ID(ByVal vCreditAgent_ID As Long)
    varCreditAgent_ID = vCreditAgent_ID
End Property

Public Property Get CreditAgent_ID() As Long
    CreditAgent_ID = varCreditAgent_ID
End Property

Public Property Let PaymentMethod_Id(ByVal vPaymentMethod_Id As Long)
    varPaymentMethod_Id = vPaymentMethod_Id
End Property

Public Property Get PaymentMethod_Id() As Long
    PaymentMethod_Id = varPaymentMethod_Id
End Property

Public Property Let BillPrinted(ByVal vBillPrinted As Boolean)
    varBillPrinted = vBillPrinted
End Property

Public Property Get BillPrinted() As Boolean
    BillPrinted = varBillPrinted
End Property

Public Property Let RepayDate(ByVal vRepayDate As Date)
    varRepayDate = vRepayDate
End Property

Public Property Get RepayDate() As Date
    RepayDate = varRepayDate
End Property

Public Property Let RepayTime(ByVal vRepayTime As Date)
    varRepayTime = vRepayTime
End Property

Public Property Get RepayTime() As Date
    RepayTime = varRepayTime
End Property

Public Property Let RepayComment(ByVal vRepayComment As String)
    varRepayComment = vRepayComment
End Property

Public Property Get RepayComment() As String
    RepayComment = varRepayComment
End Property

Public Property Let SettleCashDate(ByVal vSettleCashDate As Date)
    varSettleCashDate = vSettleCashDate
End Property

Public Property Get SettleCashDate() As Date
    SettleCashDate = varSettleCashDate
End Property

Public Property Let SettleCashTime(ByVal vSettleCashTime As Date)
    varSettleCashTime = vSettleCashTime
End Property

Public Property Get SettleCashTime() As Date
    SettleCashTime = varSettleCashTime
End Property

Public Property Let C(ByVal vC As Boolean)
    varC = vC
End Property

Public Property Get C() As Boolean
    C = varC
End Property

Public Property Let IsADoctor(ByVal vIsADoctor As Boolean)
    varIsADoctor = vIsADoctor
End Property

Public Property Get IsADoctor() As Boolean
    IsADoctor = varIsADoctor
End Property

Public Property Let IsAStaffMember(ByVal vIsAStaffMember As Boolean)
    varIsAStaffMember = vIsAStaffMember
End Property

Public Property Get IsAStaffMember() As Boolean
    IsAStaffMember = varIsAStaffMember
End Property

Public Property Let IsAnInvestigation(ByVal vIsAnInvestigation As Boolean)
    varIsAnInvestigation = vIsAnInvestigation
End Property

Public Property Get IsAnInvestigation() As Boolean
    IsAnInvestigation = varIsAnInvestigation
End Property

Public Property Let AgentRefNo(ByVal vAgentRefNo As String)
    varAgentRefNo = vAgentRefNo
End Property

Public Property Get AgentRefNo() As String
    AgentRefNo = varAgentRefNo
End Property

Public Property Let PatientAbsent(ByVal vPatientAbsent As Boolean)
    varPatientAbsent = vPatientAbsent
End Property

Public Property Get PatientAbsent() As Boolean
    PatientAbsent = varPatientAbsent
End Property

Public Property Let PatientAbsentNull(ByVal vPatientAbsentNull As Long)
    varPatientAbsentNull = vPatientAbsentNull
End Property

Public Property Get PatientAbsentNull() As Long
    PatientAbsentNull = varPatientAbsentNull
End Property

Public Property Let PersonalDue(ByVal vPersonalDue As Double)
    varPersonalDue = vPersonalDue
End Property

Public Property Get PersonalDue() As Double
    PersonalDue = varPersonalDue
End Property

Public Property Let InstitutionDue(ByVal vInstitutionDue As Double)
    varInstitutionDue = vInstitutionDue
End Property

Public Property Get InstitutionDue() As Double
    InstitutionDue = varInstitutionDue
End Property

Public Property Let OtherDue(ByVal vOtherDue As Double)
    varOtherDue = vOtherDue
End Property

Public Property Get OtherDue() As Double
    OtherDue = varOtherDue
End Property

Public Property Let TotalDue(ByVal vTotalDue As Double)
    varTotalDue = vTotalDue
End Property

Public Property Get TotalDue() As Double
    TotalDue = varTotalDue
End Property

Public Property Let AgentValidateStaffID(ByVal vAgentValidateStaffID As Long)
    varAgentValidateStaffID = vAgentValidateStaffID
End Property

Public Property Get AgentValidateStaffID() As Long
    AgentValidateStaffID = varAgentValidateStaffID
End Property

Public Property Let AgentValidateDate(ByVal vAgentValidateDate As Date)
    varAgentValidateDate = vAgentValidateDate
End Property

Public Property Get AgentValidateDate() As Date
    AgentValidateDate = varAgentValidateDate
End Property

Public Property Let AgentValidateTime(ByVal vAgentValidateTime As Date)
    varAgentValidateTime = vAgentValidateTime
End Property

Public Property Get AgentValidateTime() As Date
    AgentValidateTime = varAgentValidateTime
End Property

Public Property Let IsAForeigner(ByVal vIsAForeigner As Boolean)
    varIsAForeigner = vIsAForeigner
End Property

Public Property Get IsAForeigner() As Boolean
    IsAForeigner = varIsAForeigner
End Property
