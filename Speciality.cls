VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Speciality"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Speciality"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Dim temSQL As String
Private varSpeciality_ID As Long
Private varSpeciality As String

Private Sub clearData()
varSpeciality_ID = 0
varSpeciality = Empty
End Sub

Public Property Let Speciality_ID(ByVal vSpeciality_ID As Long)
Call clearData
    varSpeciality_ID = vSpeciality_ID
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblSpeciality WHERE Speciality_ID = " & varSpeciality_ID
        .Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
    If Not IsNull(!Speciality_ID) Then
            varSpeciality_ID = !Speciality_ID
    End If
    If Not IsNull(!Speciality) Then
            varSpeciality = !Speciality
    End If
        End If
 If .State = 1 Then .Close
    End With
    
End Property

Public Property Get Speciality_ID() As Long
    Speciality_ID = varSpeciality_ID
End Property

Public Property Let Speciality(ByVal vSpeciality As String)
    varSpeciality = vSpeciality
End Property

Public Property Get Speciality() As String
    Speciality = varSpeciality
End Property


