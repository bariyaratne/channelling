VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Doctor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Dim temSQL As String
Private varDoctor_ID As Long
Private varDoctorTitle_ID As Long
Private varDoctorSex_ID As Long
Private varDoctorName As String
Private varDoctorListedName As String
Private varDoctorQualifications As String
Private varDoctorRegistation As String
Private varDoctorDesignation As String
Private varDoctorSpeciality_ID As Long
Private varDoctorPrivateAddress As String
Private varDoctorPrivatePhone As String
Private varDoctorPrivateFax As String
Private varDoctorPrivateEmail As String
Private varDoctorMobilePhone As String
Private varDoctorOfficialAddress As String
Private varDoctorOfficialPhone As String
Private varDoctorOfficialFax As String
Private varDoctorOfficialEmail As String
Private varDoctorWebsite As String
Private varDoctorComments As String
Private varDoctorPaymentMethod_Id As Long
Private varDoctorBank_Id As Long
Private varDoctorBankBranch As String
Private varDoctorAccount As String
Private varDoctorCredit As Double
Private varDoctorCurrentlyChanneling As Boolean
Private varDoctorPhoto As String
Private varToday As Date
Private varNoOfDaysToList As Long
Private mvarNameWithTitle As String
Private mvarListedNameWithTitle As String

Public Property Let ListedNameWithTitle(ByVal vData As String)
    mvarListedNameWithTitle = vData
End Property


Public Property Get ListedNameWithTitle() As String
    Dim docTitle As New Title
    docTitle.Title_ID = varDoctorTitle_ID
    ListedNameWithTitle = docTitle.Title & " " & varDoctorListedName
End Property


Public Property Let NameWithTitle(ByVal vData As String)
    mvarNameWithTitle = vData
End Property


Public Property Get NameWithTitle() As String
    Dim docTitle As New Title
    docTitle.Title_ID = varDoctorTitle_ID
    NameWithTitle = docTitle.Title & " " & varDoctorName
End Property




Private Sub clearData()
varDoctor_ID = 0
varDoctorTitle_ID = 0
varDoctorSex_ID = 0
varDoctorName = Empty
varDoctorListedName = Empty
varDoctorQualifications = Empty
varDoctorRegistation = Empty
varDoctorDesignation = Empty
varDoctorSpeciality_ID = 0
varDoctorPrivateAddress = Empty
varDoctorPrivatePhone = Empty
varDoctorPrivateFax = Empty
varDoctorPrivateEmail = Empty
varDoctorMobilePhone = Empty
varDoctorOfficialAddress = Empty
varDoctorOfficialPhone = Empty
varDoctorOfficialFax = Empty
varDoctorOfficialEmail = Empty
varDoctorWebsite = Empty
varDoctorComments = Empty
varDoctorPaymentMethod_Id = 0
varDoctorBank_Id = 0
varDoctorBankBranch = Empty
varDoctorAccount = Empty
varDoctorCredit = 0
varDoctorCurrentlyChanneling = False
varDoctorPhoto = Empty
varToday = Empty
varNoOfDaysToList = 0
End Sub

Public Property Let Doctor_ID(ByVal vDoctor_ID As Long)
Call clearData
    varDoctor_ID = vDoctor_ID
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblDoctor WHERE Doctor_ID = " & varDoctor_ID
        .Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
    If Not IsNull(!Doctor_ID) Then
            varDoctor_ID = !Doctor_ID
    End If
    If Not IsNull(!DoctorTitle_ID) Then
            varDoctorTitle_ID = !DoctorTitle_ID
    End If
    If Not IsNull(!DoctorSex_ID) Then
            varDoctorSex_ID = !DoctorSex_ID
    End If
    If Not IsNull(!DoctorName) Then
            varDoctorName = !DoctorName
    End If
    If Not IsNull(!DoctorListedName) Then
            varDoctorListedName = !DoctorListedName
    End If
    If Not IsNull(!DoctorQualifications) Then
            varDoctorQualifications = !DoctorQualifications
    End If
    If Not IsNull(!DoctorRegistation) Then
            varDoctorRegistation = !DoctorRegistation
    End If
    If Not IsNull(!DoctorDesignation) Then
            varDoctorDesignation = !DoctorDesignation
    End If
    If Not IsNull(!DoctorSpeciality_ID) Then
            varDoctorSpeciality_ID = !DoctorSpeciality_ID
    End If
    If Not IsNull(!DoctorPrivateAddress) Then
            varDoctorPrivateAddress = !DoctorPrivateAddress
    End If
    If Not IsNull(!DoctorPrivatePhone) Then
            varDoctorPrivatePhone = !DoctorPrivatePhone
    End If
    If Not IsNull(!DoctorPrivateFax) Then
            varDoctorPrivateFax = !DoctorPrivateFax
    End If
    If Not IsNull(!DoctorPrivateEmail) Then
            varDoctorPrivateEmail = !DoctorPrivateEmail
    End If
    If Not IsNull(!DoctorMobilePhone) Then
            varDoctorMobilePhone = !DoctorMobilePhone
    End If
    If Not IsNull(!DoctorOfficialAddress) Then
            varDoctorOfficialAddress = !DoctorOfficialAddress
    End If
    If Not IsNull(!DoctorOfficialPhone) Then
            varDoctorOfficialPhone = !DoctorOfficialPhone
    End If
    If Not IsNull(!DoctorOfficialFax) Then
            varDoctorOfficialFax = !DoctorOfficialFax
    End If
    If Not IsNull(!DoctorOfficialEmail) Then
            varDoctorOfficialEmail = !DoctorOfficialEmail
    End If
    If Not IsNull(!DoctorWebsite) Then
            varDoctorWebsite = !DoctorWebsite
    End If
    If Not IsNull(!DoctorComments) Then
            varDoctorComments = !DoctorComments
    End If
    If Not IsNull(!DoctorPaymentMethod_Id) Then
            varDoctorPaymentMethod_Id = !DoctorPaymentMethod_Id
    End If
    If Not IsNull(!DoctorBank_Id) Then
            varDoctorBank_Id = !DoctorBank_Id
    End If
    If Not IsNull(!DoctorBankBranch) Then
            varDoctorBankBranch = !DoctorBankBranch
    End If
    If Not IsNull(!DoctorAccount) Then
            varDoctorAccount = !DoctorAccount
    End If
    If Not IsNull(!DoctorCredit) Then
            varDoctorCredit = !DoctorCredit
    End If
    If Not IsNull(!DoctorCurrentlyChanneling) Then
            varDoctorCurrentlyChanneling = !DoctorCurrentlyChanneling
    End If
    If Not IsNull(!DoctorPhoto) Then
            varDoctorPhoto = !DoctorPhoto
    End If
    If Not IsNull(!Today) Then
            varToday = !Today
    End If
    If Not IsNull(!NoOfDaysToList) Then
            varNoOfDaysToList = !NoOfDaysToList
    End If
        End If
 If .State = 1 Then .Close
    End With
    
End Property

Public Property Get Doctor_ID() As Long
    Doctor_ID = varDoctor_ID
End Property

Public Property Let DoctorTitle_ID(ByVal vDoctorTitle_ID As Long)
    varDoctorTitle_ID = vDoctorTitle_ID
End Property

Public Property Get DoctorTitle_ID() As Long
    DoctorTitle_ID = varDoctorTitle_ID
End Property

Public Property Let DoctorSex_ID(ByVal vDoctorSex_ID As Long)
    varDoctorSex_ID = vDoctorSex_ID
End Property

Public Property Get DoctorSex_ID() As Long
    DoctorSex_ID = varDoctorSex_ID
End Property

Public Property Let DoctorName(ByVal vDoctorName As String)
    varDoctorName = vDoctorName
End Property

Public Property Get DoctorName() As String
    DoctorName = varDoctorName
End Property

Public Property Let DoctorListedName(ByVal vDoctorListedName As String)
    varDoctorListedName = vDoctorListedName
End Property

Public Property Get DoctorListedName() As String
    DoctorListedName = varDoctorListedName
End Property

Public Property Let DoctorQualifications(ByVal vDoctorQualifications As String)
    varDoctorQualifications = vDoctorQualifications
End Property

Public Property Get DoctorQualifications() As String
    DoctorQualifications = varDoctorQualifications
End Property

Public Property Let DoctorRegistation(ByVal vDoctorRegistation As String)
    varDoctorRegistation = vDoctorRegistation
End Property

Public Property Get DoctorRegistation() As String
    DoctorRegistation = varDoctorRegistation
End Property

Public Property Let DoctorDesignation(ByVal vDoctorDesignation As String)
    varDoctorDesignation = vDoctorDesignation
End Property

Public Property Get DoctorDesignation() As String
    DoctorDesignation = varDoctorDesignation
End Property

Public Property Let DoctorSpeciality_ID(ByVal vDoctorSpeciality_ID As Long)
    varDoctorSpeciality_ID = vDoctorSpeciality_ID
End Property

Public Property Get DoctorSpeciality_ID() As Long
    DoctorSpeciality_ID = varDoctorSpeciality_ID
End Property

Public Property Let DoctorPrivateAddress(ByVal vDoctorPrivateAddress As String)
    varDoctorPrivateAddress = vDoctorPrivateAddress
End Property

Public Property Get DoctorPrivateAddress() As String
    DoctorPrivateAddress = varDoctorPrivateAddress
End Property

Public Property Let DoctorPrivatePhone(ByVal vDoctorPrivatePhone As String)
    varDoctorPrivatePhone = vDoctorPrivatePhone
End Property

Public Property Get DoctorPrivatePhone() As String
    DoctorPrivatePhone = varDoctorPrivatePhone
End Property

Public Property Let DoctorPrivateFax(ByVal vDoctorPrivateFax As String)
    varDoctorPrivateFax = vDoctorPrivateFax
End Property

Public Property Get DoctorPrivateFax() As String
    DoctorPrivateFax = varDoctorPrivateFax
End Property

Public Property Let DoctorPrivateEmail(ByVal vDoctorPrivateEmail As String)
    varDoctorPrivateEmail = vDoctorPrivateEmail
End Property

Public Property Get DoctorPrivateEmail() As String
    DoctorPrivateEmail = varDoctorPrivateEmail
End Property

Public Property Let DoctorMobilePhone(ByVal vDoctorMobilePhone As String)
    varDoctorMobilePhone = vDoctorMobilePhone
End Property

Public Property Get DoctorMobilePhone() As String
    DoctorMobilePhone = varDoctorMobilePhone
End Property

Public Property Let DoctorOfficialAddress(ByVal vDoctorOfficialAddress As String)
    varDoctorOfficialAddress = vDoctorOfficialAddress
End Property

Public Property Get DoctorOfficialAddress() As String
    DoctorOfficialAddress = varDoctorOfficialAddress
End Property

Public Property Let DoctorOfficialPhone(ByVal vDoctorOfficialPhone As String)
    varDoctorOfficialPhone = vDoctorOfficialPhone
End Property

Public Property Get DoctorOfficialPhone() As String
    DoctorOfficialPhone = varDoctorOfficialPhone
End Property

Public Property Let DoctorOfficialFax(ByVal vDoctorOfficialFax As String)
    varDoctorOfficialFax = vDoctorOfficialFax
End Property

Public Property Get DoctorOfficialFax() As String
    DoctorOfficialFax = varDoctorOfficialFax
End Property

Public Property Let DoctorOfficialEmail(ByVal vDoctorOfficialEmail As String)
    varDoctorOfficialEmail = vDoctorOfficialEmail
End Property

Public Property Get DoctorOfficialEmail() As String
    DoctorOfficialEmail = varDoctorOfficialEmail
End Property

Public Property Let DoctorWebsite(ByVal vDoctorWebsite As String)
    varDoctorWebsite = vDoctorWebsite
End Property

Public Property Get DoctorWebsite() As String
    DoctorWebsite = varDoctorWebsite
End Property

Public Property Let DoctorComments(ByVal vDoctorComments As String)
    varDoctorComments = vDoctorComments
End Property

Public Property Get DoctorComments() As String
    DoctorComments = varDoctorComments
End Property

Public Property Let DoctorPaymentMethod_Id(ByVal vDoctorPaymentMethod_Id As Long)
    varDoctorPaymentMethod_Id = vDoctorPaymentMethod_Id
End Property

Public Property Get DoctorPaymentMethod_Id() As Long
    DoctorPaymentMethod_Id = varDoctorPaymentMethod_Id
End Property

Public Property Let DoctorBank_Id(ByVal vDoctorBank_Id As Long)
    varDoctorBank_Id = vDoctorBank_Id
End Property

Public Property Get DoctorBank_Id() As Long
    DoctorBank_Id = varDoctorBank_Id
End Property

Public Property Let DoctorBankBranch(ByVal vDoctorBankBranch As String)
    varDoctorBankBranch = vDoctorBankBranch
End Property

Public Property Get DoctorBankBranch() As String
    DoctorBankBranch = varDoctorBankBranch
End Property

Public Property Let DoctorAccount(ByVal vDoctorAccount As String)
    varDoctorAccount = vDoctorAccount
End Property

Public Property Get DoctorAccount() As String
    DoctorAccount = varDoctorAccount
End Property

Public Property Let DoctorCredit(ByVal vDoctorCredit As Double)
    varDoctorCredit = vDoctorCredit
End Property

Public Property Get DoctorCredit() As Double
    DoctorCredit = varDoctorCredit
End Property

Public Property Let DoctorCurrentlyChanneling(ByVal vDoctorCurrentlyChanneling As Boolean)
    varDoctorCurrentlyChanneling = vDoctorCurrentlyChanneling
End Property

Public Property Get DoctorCurrentlyChanneling() As Boolean
    DoctorCurrentlyChanneling = varDoctorCurrentlyChanneling
End Property

Public Property Let DoctorPhoto(ByVal vDoctorPhoto As String)
    varDoctorPhoto = vDoctorPhoto
End Property

Public Property Get DoctorPhoto() As String
    DoctorPhoto = varDoctorPhoto
End Property

Public Property Let Today(ByVal vToday As Date)
    varToday = vToday
End Property

Public Property Get Today() As Date
    Today = varToday
End Property

Public Property Let NoOfDaysToList(ByVal vNoOfDaysToList As Long)
    varNoOfDaysToList = vNoOfDaysToList
End Property

Public Property Get NoOfDaysToList() As Long
    NoOfDaysToList = varNoOfDaysToList
End Property


