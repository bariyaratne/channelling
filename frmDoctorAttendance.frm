VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmDoctorAttendance 
   Caption         =   "Doctors Available"
   ClientHeight    =   7785
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   14040
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7785
   ScaleWidth      =   14040
   Begin MSDataListLib.DataList lstSearch 
      Height          =   2790
      Left            =   10560
      TabIndex        =   7
      Top             =   1200
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   4921
      _Version        =   393216
   End
   Begin VB.TextBox txtSearch 
      Height          =   375
      Left            =   10560
      TabIndex        =   6
      Top             =   840
      Width           =   3015
   End
   Begin TabDlg.SSTab ssDoc 
      Height          =   7335
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   10245
      _ExtentX        =   18071
      _ExtentY        =   12938
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Currently Available"
      TabPicture(0)   =   "frmDoctorAttendance.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "gridPresent"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Already Left"
      TabPicture(1)   =   "frmDoctorAttendance.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "gridLeft"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin MSFlexGridLib.MSFlexGrid gridPresent 
         Height          =   6735
         Left            =   120
         TabIndex        =   4
         Top             =   480
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   11880
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid gridLeft 
         Height          =   6735
         Left            =   -74880
         TabIndex        =   5
         Top             =   480
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   11880
         _Version        =   393216
      End
   End
   Begin VB.TextBox txtDetail 
      Height          =   615
      Left            =   10560
      TabIndex        =   2
      Top             =   6840
      Width           =   3135
   End
   Begin VB.CommandButton btnArrive 
      Caption         =   "Mark as Arrived"
      Height          =   495
      Left            =   10560
      TabIndex        =   1
      Top             =   4080
      Width           =   3015
   End
   Begin VB.CommandButton btnLeave 
      Caption         =   "Mark as Left"
      Height          =   495
      Left            =   10560
      TabIndex        =   0
      Top             =   240
      Width           =   3015
   End
End
Attribute VB_Name = "frmDoctorAttendance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim docs As New clsFillCombo
    Dim sql As String


Private Sub btnArrive_Click()
    If IsNumeric(lstSearch.BoundText) = False Then
        MsgBox "Doctor ?"
        lstSearch.SetFocus
        Exit Sub
    End If
    Dim docAtt As New clsDoctorAttendance
    docAtt.doctorId = Val(lstSearch.BoundText)
    docAtt.onTime = Now
    docAtt.onUserId = UserID
    docAtt.saveData
    fillDoctors
    MsgBox "Marked as Arrived"
End Sub

Public Sub fillDoctors()
    sql = "SELECT tblDoctorAttendance.ID, tblSpeciality.Speciality, tblTitle.Title & ' ' & tblDoctor.DoctorName as [Doctor Name], Format([tblDoctorAttendance].[onTime],'dd mmmm yyyy hh:nn ampm') AS ArrivedTime " & _
            "FROM tblDoctorAttendance LEFT JOIN ((tblDoctor LEFT JOIN tblSpeciality ON tblDoctor.DoctorSpeciality_ID = tblSpeciality.Speciality_ID) LEFT JOIN tblTitle ON tblDoctor.DoctorTitle_ID = tblTitle.Title_ID) ON tblDoctorAttendance.doctorId = tblDoctor.Doctor_ID " & _
            "Where (((tblDoctorAttendance.offUserId) = 0)) " & _
            "ORDER BY tblSpeciality.Speciality, tblDoctor.DoctorName"
    Dim a(0) As Integer
    FillAnyGrid sql, gridPresent, 0, a, a
    With gridPresent
        .ColWidth(0) = 0
        .ColWidth(1) = 2600
        .ColWidth(2) = 4600
        .ColWidth(3) = 2500
    End With
End Sub


Private Sub btnLeave_Click()
    Dim id As Long
    id = Val(gridPresent.TextMatrix(gridPresent.Row, 0))
    
    Dim docAtt As New clsDoctorAttendance
    docAtt.id = id
    docAtt.loadData
    docAtt.offTime = Now
    docAtt.offUserId = UserID
    docAtt.saveData
    fillDoctors
    MsgBox "Marked as Left"

    
End Sub

Private Sub Form_Load()
    txtSearch_Change
    fillDoctors
End Sub

Private Sub txtSearch_Change()
    If txtSearch.Text <> "" Then
        sql = "select Doctor_id, Doctorname from tblDoctor where DoctorName like '%" & txtSearch.Text & "%' order by DoctorName"
    Else
        sql = "select Doctor_id, Doctorname  from tblDoctor order by DoctorName"
    End If
    docs.fillSqlList lstSearch, sql
End Sub


Private Sub txtSearch_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 222 Then
        KeyCode = Empty
    ElseIf KeyCode = vbKeyEscape Then
        txtSearch.Text = Empty
    ElseIf KeyCode = vbKeyReturn Then
        btnArrive_Click
        KeyCode = Empty
    ElseIf KeyCode = vbKeyDown Then
        lstSearch.SetFocus
    ElseIf KeyCode = vbKeyUp Then
        ssDoc.SetFocus
        
        KeyCode = Empty
    ElseIf KeyCode = vbKeyLeft Then
        
        lstSearch.SetFocus
        KeyCode = Empty
    End If
End Sub


Private Sub txtSearch_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 222 Then
        KeyCode = Empty
    End If
End Sub


Private Sub lstSearch_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        btnArrive_Click
        KeyCode = Empty
    ElseIf KeyCode = vbKeyEscape Then
        txtSearch.SetFocus
    End If
End Sub


