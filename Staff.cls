VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Staff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varStaff_ID As Long
    Private varStaffTitle_ID As Long
    Private varStaffSex_ID As Long
    Private varStaffAuthority As Long
    Private varStaffName As String
    Private varStaffListedName As String
    Private varStaffQualifications As String
    Private varStaffRegistation As String
    Private varStaffDesignation As String
    Private varStaffSpeciality_ID As Long
    Private varStaffAuthority_ID As Long
    Private varStaffPrivateAddress As String
    Private varStaffPrivatePhone As String
    Private varStaffPrivateFax As String
    Private varStaffPrivateEmail As String
    Private varStaffMobilePhone As String
    Private varStaffOfficialAddress As String
    Private varStaffOfficialPhone As String
    Private varStaffOfficialFax As String
    Private varStaffOfficialEmail As String
    Private varStaffWebsite As String
    Private varStaffComments As String
    Private varStaffPaymentMethod_Id As Long
    Private varStaffBank_Id As Long
    Private varStaffBankBranch As String
    Private varStaffAccount As String
    Private varStaffCredit As Double
    Private varStaffCurrentlyChanneling As Boolean
    Private varStaffPhoto As String
    Private varStaffNextOfKin As String
    Private varStaffNextOfKinDetails As String
    Private varStaffUserName As String
    Private varStaffPassword As String
    Private varStaffUser As Boolean
    Private varlOGGED As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblStaff"
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If varStaff_ID = 0 Then .AddNew
        !StaffTitle_ID = varStaffTitle_ID
        !StaffSex_ID = varStaffSex_ID
        !StaffAuthority = varStaffAuthority
        !StaffName = varStaffName
        !StaffListedName = varStaffListedName
        !StaffQualifications = varStaffQualifications
        !StaffRegistation = varStaffRegistation
        !StaffDesignation = varStaffDesignation
        !StaffSpeciality_ID = varStaffSpeciality_ID
        !StaffAuthority_ID = varStaffAuthority_ID
        !StaffPrivateAddress = varStaffPrivateAddress
        !StaffPrivatePhone = varStaffPrivatePhone
        !StaffPrivateFax = varStaffPrivateFax
        !StaffPrivateEmail = varStaffPrivateEmail
        !StaffMobilePhone = varStaffMobilePhone
        !StaffOfficialAddress = varStaffOfficialAddress
        !StaffOfficialPhone = varStaffOfficialPhone
        !StaffOfficialFax = varStaffOfficialFax
        !StaffOfficialEmail = varStaffOfficialEmail
        !StaffWebsite = varStaffWebsite
        !StaffComments = varStaffComments
        !StaffPaymentMethod_Id = varStaffPaymentMethod_Id
        !StaffBank_Id = varStaffBank_Id
        !StaffBankBranch = varStaffBankBranch
        !StaffAccount = varStaffAccount
        !StaffCredit = varStaffCredit
        !StaffCurrentlyChanneling = varStaffCurrentlyChanneling
        !StaffPhoto = varStaffPhoto
        !StaffNextOfKin = varStaffNextOfKin
        !StaffNextOfKinDetails = varStaffNextOfKinDetails
        !StaffUserName = varStaffUserName
        !StaffPassword = varStaffPassword
        !StaffUser = varStaffUser
        !lOGGED = varlOGGED
        .Update
        varStaff_ID = !Staff_ID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblStaff WHERE Staff_ID = " & varStaff_ID
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If Not IsNull(!Staff_ID) Then
           varStaff_ID = !Staff_ID
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffTitle_ID = !StaffTitle_ID
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffSex_ID = !StaffSex_ID
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffAuthority = !StaffAuthority
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffName = !StaffName
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffListedName = !StaffListedName
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffQualifications = !StaffQualifications
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffRegistation = !StaffRegistation
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffDesignation = !StaffDesignation
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffSpeciality_ID = !StaffSpeciality_ID
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffAuthority_ID = !StaffAuthority_ID
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffPrivateAddress = !StaffPrivateAddress
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffPrivatePhone = !StaffPrivatePhone
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffPrivateFax = !StaffPrivateFax
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffPrivateEmail = !StaffPrivateEmail
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffMobilePhone = !StaffMobilePhone
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffOfficialAddress = !StaffOfficialAddress
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffOfficialPhone = !StaffOfficialPhone
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffOfficialFax = !StaffOfficialFax
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffOfficialEmail = !StaffOfficialEmail
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffWebsite = !StaffWebsite
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffComments = !StaffComments
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffPaymentMethod_Id = !StaffPaymentMethod_Id
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffBank_Id = !StaffBank_Id
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffBankBranch = !StaffBankBranch
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffAccount = !StaffAccount
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffCredit = !StaffCredit
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffCurrentlyChanneling = !StaffCurrentlyChanneling
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffPhoto = !StaffPhoto
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffNextOfKin = !StaffNextOfKin
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffNextOfKinDetails = !StaffNextOfKinDetails
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffUserName = !StaffUserName
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffPassword = !StaffPassword
        End If
        If Not IsNull(!Staff_ID) Then
           varStaffUser = !StaffUser
        End If
        If Not IsNull(!Staff_ID) Then
           varlOGGED = !lOGGED
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varStaff_ID = 0
    varStaffTitle_ID = 0
    varStaffSex_ID = 0
    varStaffAuthority = 0
    varStaffName = Empty
    varStaffListedName = Empty
    varStaffQualifications = Empty
    varStaffRegistation = Empty
    varStaffDesignation = Empty
    varStaffSpeciality_ID = 0
    varStaffAuthority_ID = 0
    varStaffPrivateAddress = Empty
    varStaffPrivatePhone = Empty
    varStaffPrivateFax = Empty
    varStaffPrivateEmail = Empty
    varStaffMobilePhone = Empty
    varStaffOfficialAddress = Empty
    varStaffOfficialPhone = Empty
    varStaffOfficialFax = Empty
    varStaffOfficialEmail = Empty
    varStaffWebsite = Empty
    varStaffComments = Empty
    varStaffPaymentMethod_Id = 0
    varStaffBank_Id = 0
    varStaffBankBranch = Empty
    varStaffAccount = Empty
    varStaffCredit = 0
    varStaffCurrentlyChanneling = False
    varStaffPhoto = Empty
    varStaffNextOfKin = Empty
    varStaffNextOfKinDetails = Empty
    varStaffUserName = Empty
    varStaffPassword = Empty
    varStaffUser = False
    varlOGGED = False
End Sub

Public Property Let Staff_ID(ByVal vStaff_ID As Long)
    Call clearData
    varStaff_ID = vStaff_ID
    Call loadData
End Property

Public Property Get Staff_ID() As Long
    Staff_ID = varStaff_ID
End Property

Public Property Let StaffTitle_ID(ByVal vStaffTitle_ID As Long)
    varStaffTitle_ID = vStaffTitle_ID
End Property

Public Property Get StaffTitle_ID() As Long
    StaffTitle_ID = varStaffTitle_ID
End Property

Public Property Let StaffSex_ID(ByVal vStaffSex_ID As Long)
    varStaffSex_ID = vStaffSex_ID
End Property

Public Property Get StaffSex_ID() As Long
    StaffSex_ID = varStaffSex_ID
End Property

Public Property Let StaffAuthority(ByVal vStaffAuthority As Long)
    varStaffAuthority = vStaffAuthority
End Property

Public Property Get StaffAuthority() As Long
    StaffAuthority = varStaffAuthority
End Property

Public Property Let StaffName(ByVal vStaffName As String)
    varStaffName = vStaffName
End Property

Public Property Get StaffName() As String
    StaffName = varStaffName
End Property

Public Property Let StaffListedName(ByVal vStaffListedName As String)
    varStaffListedName = vStaffListedName
End Property

Public Property Get StaffListedName() As String
    StaffListedName = varStaffListedName
End Property

Public Property Let StaffQualifications(ByVal vStaffQualifications As String)
    varStaffQualifications = vStaffQualifications
End Property

Public Property Get StaffQualifications() As String
    StaffQualifications = varStaffQualifications
End Property

Public Property Let StaffRegistation(ByVal vStaffRegistation As String)
    varStaffRegistation = vStaffRegistation
End Property

Public Property Get StaffRegistation() As String
    StaffRegistation = varStaffRegistation
End Property

Public Property Let StaffDesignation(ByVal vStaffDesignation As String)
    varStaffDesignation = vStaffDesignation
End Property

Public Property Get StaffDesignation() As String
    StaffDesignation = varStaffDesignation
End Property

Public Property Let StaffSpeciality_ID(ByVal vStaffSpeciality_ID As Long)
    varStaffSpeciality_ID = vStaffSpeciality_ID
End Property

Public Property Get StaffSpeciality_ID() As Long
    StaffSpeciality_ID = varStaffSpeciality_ID
End Property

Public Property Let StaffAuthority_ID(ByVal vStaffAuthority_ID As Long)
    varStaffAuthority_ID = vStaffAuthority_ID
End Property

Public Property Get StaffAuthority_ID() As Long
    StaffAuthority_ID = varStaffAuthority_ID
End Property

Public Property Let StaffPrivateAddress(ByVal vStaffPrivateAddress As String)
    varStaffPrivateAddress = vStaffPrivateAddress
End Property

Public Property Get StaffPrivateAddress() As String
    StaffPrivateAddress = varStaffPrivateAddress
End Property

Public Property Let StaffPrivatePhone(ByVal vStaffPrivatePhone As String)
    varStaffPrivatePhone = vStaffPrivatePhone
End Property

Public Property Get StaffPrivatePhone() As String
    StaffPrivatePhone = varStaffPrivatePhone
End Property

Public Property Let StaffPrivateFax(ByVal vStaffPrivateFax As String)
    varStaffPrivateFax = vStaffPrivateFax
End Property

Public Property Get StaffPrivateFax() As String
    StaffPrivateFax = varStaffPrivateFax
End Property

Public Property Let StaffPrivateEmail(ByVal vStaffPrivateEmail As String)
    varStaffPrivateEmail = vStaffPrivateEmail
End Property

Public Property Get StaffPrivateEmail() As String
    StaffPrivateEmail = varStaffPrivateEmail
End Property

Public Property Let StaffMobilePhone(ByVal vStaffMobilePhone As String)
    varStaffMobilePhone = vStaffMobilePhone
End Property

Public Property Get StaffMobilePhone() As String
    StaffMobilePhone = varStaffMobilePhone
End Property

Public Property Let StaffOfficialAddress(ByVal vStaffOfficialAddress As String)
    varStaffOfficialAddress = vStaffOfficialAddress
End Property

Public Property Get StaffOfficialAddress() As String
    StaffOfficialAddress = varStaffOfficialAddress
End Property

Public Property Let StaffOfficialPhone(ByVal vStaffOfficialPhone As String)
    varStaffOfficialPhone = vStaffOfficialPhone
End Property

Public Property Get StaffOfficialPhone() As String
    StaffOfficialPhone = varStaffOfficialPhone
End Property

Public Property Let StaffOfficialFax(ByVal vStaffOfficialFax As String)
    varStaffOfficialFax = vStaffOfficialFax
End Property

Public Property Get StaffOfficialFax() As String
    StaffOfficialFax = varStaffOfficialFax
End Property

Public Property Let StaffOfficialEmail(ByVal vStaffOfficialEmail As String)
    varStaffOfficialEmail = vStaffOfficialEmail
End Property

Public Property Get StaffOfficialEmail() As String
    StaffOfficialEmail = varStaffOfficialEmail
End Property

Public Property Let StaffWebsite(ByVal vStaffWebsite As String)
    varStaffWebsite = vStaffWebsite
End Property

Public Property Get StaffWebsite() As String
    StaffWebsite = varStaffWebsite
End Property

Public Property Let StaffComments(ByVal vStaffComments As String)
    varStaffComments = vStaffComments
End Property

Public Property Get StaffComments() As String
    StaffComments = varStaffComments
End Property

Public Property Let StaffPaymentMethod_Id(ByVal vStaffPaymentMethod_Id As Long)
    varStaffPaymentMethod_Id = vStaffPaymentMethod_Id
End Property

Public Property Get StaffPaymentMethod_Id() As Long
    StaffPaymentMethod_Id = varStaffPaymentMethod_Id
End Property

Public Property Let StaffBank_Id(ByVal vStaffBank_Id As Long)
    varStaffBank_Id = vStaffBank_Id
End Property

Public Property Get StaffBank_Id() As Long
    StaffBank_Id = varStaffBank_Id
End Property

Public Property Let StaffBankBranch(ByVal vStaffBankBranch As String)
    varStaffBankBranch = vStaffBankBranch
End Property

Public Property Get StaffBankBranch() As String
    StaffBankBranch = varStaffBankBranch
End Property

Public Property Let StaffAccount(ByVal vStaffAccount As String)
    varStaffAccount = vStaffAccount
End Property

Public Property Get StaffAccount() As String
    StaffAccount = varStaffAccount
End Property

Public Property Let StaffCredit(ByVal vStaffCredit As Double)
    varStaffCredit = vStaffCredit
End Property

Public Property Get StaffCredit() As Double
    StaffCredit = varStaffCredit
End Property

Public Property Let StaffCurrentlyChanneling(ByVal vStaffCurrentlyChanneling As Boolean)
    varStaffCurrentlyChanneling = vStaffCurrentlyChanneling
End Property

Public Property Get StaffCurrentlyChanneling() As Boolean
    StaffCurrentlyChanneling = varStaffCurrentlyChanneling
End Property

Public Property Let StaffPhoto(ByVal vStaffPhoto As String)
    varStaffPhoto = vStaffPhoto
End Property

Public Property Get StaffPhoto() As String
    StaffPhoto = varStaffPhoto
End Property

Public Property Let StaffNextOfKin(ByVal vStaffNextOfKin As String)
    varStaffNextOfKin = vStaffNextOfKin
End Property

Public Property Get StaffNextOfKin() As String
    StaffNextOfKin = varStaffNextOfKin
End Property

Public Property Let StaffNextOfKinDetails(ByVal vStaffNextOfKinDetails As String)
    varStaffNextOfKinDetails = vStaffNextOfKinDetails
End Property

Public Property Get StaffNextOfKinDetails() As String
    StaffNextOfKinDetails = varStaffNextOfKinDetails
End Property

Public Property Let StaffUserName(ByVal vStaffUserName As String)
    varStaffUserName = vStaffUserName
End Property

Public Property Get StaffUserName() As String
    StaffUserName = varStaffUserName
End Property

Public Property Let StaffPassword(ByVal vStaffPassword As String)
    varStaffPassword = vStaffPassword
End Property

Public Property Get StaffPassword() As String
    StaffPassword = varStaffPassword
End Property

Public Property Let StaffUser(ByVal vStaffUser As Boolean)
    varStaffUser = vStaffUser
End Property

Public Property Get StaffUser() As Boolean
    StaffUser = varStaffUser
End Property

Public Property Let lOGGED(ByVal vlOGGED As Boolean)
    varlOGGED = vlOGGED
End Property

Public Property Get lOGGED() As Boolean
    lOGGED = varlOGGED
End Property
