VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DateSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarSessionDate As Date 'local copy
Private mvarSelectedSession As Session 'local copy
'local variable(s) to hold property value(s)
Private mvarBookedNo As Long 'local copy
Public Property Let BookedNo(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.BookedNo = 5
    mvarBookedNo = vData
End Property


Public Property Get BookedNo() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.BookedNo
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select count(HospitalFacility_ID) as TotalBookings from tblpatientfacility where hospitalfacility_ID = 10 and Staff_ID = " & mvarSelectedSession.Staff_ID & " and AppointmentDate = #" & Format(mvarSessionDate, DefaultLongDate) & "# and Secession = " & mvarSelectedSession.FacilitySecession_ID & " "
        .Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
        If IsNull(!TotalBookings) = False Then
            mvarBookedNo = !TotalBookings
        Else
            mvarBookedNo = 0
        End If
        .Close
    End With

    BookedNo = mvarBookedNo
End Property



Public Property Set SelectedSession(ByVal vData As Session)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.SelectedSession = Form1
    Set mvarSelectedSession = vData
End Property


Public Property Get SelectedSession() As Session
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SelectedSession
    Set SelectedSession = mvarSelectedSession
End Property



Public Property Let SessionDate(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SessionDate = 5
    mvarSessionDate = vData
End Property


Public Property Get SessionDate() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SessionDate
    SessionDate = mvarSessionDate
End Property



