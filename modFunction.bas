Attribute VB_Name = "modFunction"
Option Explicit
    Dim FSys As New Scripting.FileSystemObject
    Dim i As Integer
    Dim temSQL As String
    Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long


Public Function RepeatString(InputString As String, RepeatNo As Integer) As String
    Dim r As Integer
    For r = 1 To RepeatNo
        RepeatString = RepeatString & InputString
    Next r
End Function


Public Sub SaveCommonSettings(MyForm As Form)
    Dim MyCtrl As Control
    Dim i As Integer
    For Each MyCtrl In MyForm.Controls
        If TypeOf MyCtrl Is MSFlexGrid Then
            For i = 0 To MyCtrl.Cols - 1
                SaveSetting App.EXEName, MyForm.Name & MyCtrl.Name, i, MyCtrl.ColWidth(i)
            Next
        ElseIf TypeOf MyCtrl Is ComboBox Then
            If InStr(MyCtrl.Tag, "SS") Then
                SaveSetting App.EXEName, MyForm.Name, MyCtrl.Name, MyCtrl.Text
            End If
        End If
    Next
    SaveSetting App.EXEName, MyForm.Name, "Top", MyForm.Top
    SaveSetting App.EXEName, MyForm.Name, "Left", MyForm.Left
    SaveSetting App.EXEName, MyForm.Name, "Width", MyForm.Width
    SaveSetting App.EXEName, MyForm.Name, "Height", MyForm.Height
    SaveSetting App.EXEName, MyForm.Name, "WindowState", MyForm.WindowState
    
End Sub

Public Sub GetCommonSettings(MyForm As Form)
    Dim MyCtrl As Control
    Dim i As Integer
    
    For Each MyCtrl In MyForm.Controls
        If TypeOf MyCtrl Is MSFlexGrid Then
            For i = 0 To MyCtrl.Cols - 1
                MyCtrl.ColWidth(i) = GetSetting(App.EXEName, MyForm.Name & MyCtrl.Name, i, MyCtrl.ColWidth(i))
                MyCtrl.AllowUserResizing = flexResizeColumns
            Next
        ElseIf TypeOf MyCtrl Is ComboBox Then
            On Error Resume Next
            If InStr(MyCtrl.Tag, "SS") Then
                MyCtrl.Text = GetSetting(App.EXEName, MyForm.Name, MyCtrl.Name, "")
            End If
            On Error GoTo 0
        End If
    Next
    
    If Val(GetSetting(App.EXEName, MyForm.Name, "Width", MyForm.Top)) < MyForm.Height * 0.75 Then MyForm.Top = GetSetting(App.EXEName, MyForm.Name, "Top", MyForm.Top)
    If Val(GetSetting(App.EXEName, MyForm.Name, "Width", MyForm.Left)) < MyForm.Width * 0.75 Then MyForm.Left = GetSetting(App.EXEName, MyForm.Name, "Left", MyForm.Left)
    If Val(GetSetting(App.EXEName, MyForm.Name, "Width", MyForm.Width)) > 0 Then MyForm.Width = GetSetting(App.EXEName, MyForm.Name, "Width", MyForm.Width)
    If Val(GetSetting(App.EXEName, MyForm.Name, "Width", MyForm.Height)) > 0 Then MyForm.Height = GetSetting(App.EXEName, MyForm.Name, "Height", MyForm.Height)

    On Error Resume Next
    MyForm.WindowState = GetSetting(App.EXEName, MyForm.Name, "WindowState", MyForm.WindowState)

End Sub

Public Sub GridToExcel(ExportGrid As MSFlexGrid, Optional Topic As String, Optional Subtopic As String)
    If ExportGrid.Rows <= 1 Then
        MsgBox "Noting to Export"
        Exit Sub
    End If
    
    Dim AppExcel As Excel.Application
    Dim myworkbook As Excel.Workbook
    Dim myWorkSheet1 As Excel.Worksheet
    Dim temRow As Integer
    Dim temCol As Integer
    
    Set AppExcel = CreateObject("Excel.Application")
    Set myworkbook = AppExcel.Workbooks.Add
    Set myWorkSheet1 = AppExcel.Worksheets(1)
    
    myWorkSheet1.Cells(1, 1) = Topic
    myWorkSheet1.Cells(2, 1) = Subtopic
    
    For temRow = 0 To ExportGrid.Rows - 1
        For temCol = 0 To ExportGrid.Cols - 1
            myWorkSheet1.Cells(temRow + 4, temCol + 1) = ExportGrid.TextMatrix(temRow, temCol)
        Next
    Next temRow
    
    myWorkSheet1.Range("A1:" & GetColumnName(CDbl(temCol)) & temRow + 2).AutoFormat Format:=xlRangeAutoFormatClassic1
    
    myWorkSheet1.Range("A" & temRow + 3 & ":" & GetColumnName(CDbl(temCol)) & temRow + 3).AutoFormat Format:=xlRangeAutoFormat3DEffects1
    
    Topic = "Day End Summery " & Format(Date, "dd MMMM yyyy")
    myworkbook.SaveAs (App.Path & "\" & Topic & ".xls")
    myworkbook.Save
    myworkbook.Close
    
    ShellExecute 0&, "open", App.Path & "\" & Topic & ".xls", "", "", vbMaximizedFocus
End Sub

Private Function GetColumnName(ColumnNo As Long) As String
    Dim temnum As Integer
    Dim temnum1 As Integer
    
    If ColumnNo < 27 Then
        GetColumnName = Chr(ColumnNo + 64)
    Else
        temnum = ColumnNo \ 26
        temnum1 = ColumnNo Mod 26
        GetColumnName = Chr(temnum + 64) & Chr(temnum1 + 64)
    End If
End Function



'Public Sub ColourGrid(GridToColour As MSFlexGrid)
'    Dim col As Integer
'    Dim row As Integer
'    With GridToColour
'        .Visible = False
'        For row = 0 To .Rows - 1
'            For col = 0 To .Cols - 1
'                .col = col
'                .row = row
'                If .row Mod 2 = 0 Then
'                    .CellBackColor = DefaultColourScheme.GridLightBackColour
'                Else
'                    .CellBackColor = DefaultColourScheme.GridDarkBackColour
'                End If
'            Next
'        Next
'        .Visible = True
'    End With
'End Sub
'
'Public Sub SetColours(MyForm As Form)
'    MyForm.ForeColor = DefaultColourScheme.LabelForeColour
'    MyForm.BackColor = DefaultColourScheme.LabelBackColour
'    On Error Resume Next
'    Dim MyControl As Control
'    For Each MyControl In MyForm.Controls
'        If InStr(UCase(MyControl.Name), "BTN") > 0 Then
'            MyControl.ForeColor = DefaultColourScheme.ButtonForeColour
'            MyControl.BackColor = DefaultColourScheme.ButtonBackColour
'            MyControl.BorderColor = DefaultColourScheme.ButtonBorderColour
'        ElseIf InStr(UCase(MyControl.Name), "LST") > 0 Then
'            MyControl.ForeColor = DefaultColourScheme.LabelForeColour
'            MyControl.BackColor = DefaultColourScheme.LabelBackColour
'        ElseIf InStr(UCase(MyControl.Name), "TXTID") > 0 Then
'            MyControl.ForeColor = DefaultColourScheme.LabelForeColour
'            MyControl.BackColor = DefaultColourScheme.LabelBackColour
'        ElseIf InStr(UCase(MyControl.Name), "CMB") > 0 Then
'            MyControl.ForeColor = DefaultColourScheme.ComboForeColour
'            MyControl.BackColor = DefaultColourScheme.ComboBackColour
'        ElseIf InStr(UCase(MyControl.Name), "TXT") > 0 Then
'            MyControl.ForeColor = DefaultColourScheme.TextForeColour
'            MyControl.BackColor = DefaultColourScheme.TextBackColour
'        ElseIf InStr(UCase(MyControl.Name), "DTP") > 0 Then
'            MyControl.ForeColor = DefaultColourScheme.TextForeColour
'            MyControl.BackColor = DefaultColourScheme.TextBackColour
'        Else
'            MyControl.ForeColor = DefaultColourScheme.LabelForeColour
'            MyControl.BackColor = DefaultColourScheme.LabelBackColour
'            MyControl.BackStyle = 0
'        End If
'    Next
'End Sub
'
