VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Patient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varPatient_ID As Long
    Private varPatient_ID2 As String
    Private varFirstName As String
    Private varOtherNames As String
    Private varSurName As String
    Private varTitle_ID As Long
    Private varSex_ID As Long
    Private varRace_ID As Long
    Private varMarital_ID As Long
    Private varNICNo As String
    Private varBirthCertificateNo As String
    Private varAddress As String
    Private varPhone As String
    Private varFax As String
    Private varEmail As String
    Private varDateOfBirth As Date
    Private varNotes As String
    Private varPhoto As String
    Private varCredit As Double
    Private varMaxCredit As Double
    Private varBlackListedPatient As Boolean
    Private varRegisteredDate As Date

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientMainDetails"
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If varPatient_ID = 0 Then .AddNew
        !Patient_ID2 = varPatient_ID2
        !FirstName = varFirstName
        !OtherNames = varOtherNames
        !SurName = varSurName
        !Title_ID = varTitle_ID
        !Sex_ID = varSex_ID
        !Race_ID = varRace_ID
        !Marital_ID = varMarital_ID
        !NICNo = varNICNo
        !BirthCertificateNo = varBirthCertificateNo
        !Address = varAddress
        !Phone = varPhone
        !Fax = varFax
        !Email = varEmail
        !DateOfBirth = varDateOfBirth
        !Notes = varNotes
        !Photo = varPhoto
        !Credit = varCredit
        !MaxCredit = varMaxCredit
        !BlackListedPatient = varBlackListedPatient
        !RegisteredDate = varRegisteredDate
        .Update
        varPatient_ID = !Patient_ID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientMainDetails WHERE Patient_ID = " & varPatient_ID
        .Open temSQL, cnnChanneling, adOpenStatic, adLockOptimistic
        If Not IsNull(!Patient_ID) Then
           varPatient_ID = !Patient_ID
        End If
        If Not IsNull(!Patient_ID) Then
           varPatient_ID2 = !Patient_ID2
        End If
        If Not IsNull(!Patient_ID) Then
           varFirstName = !FirstName
        End If
        If Not IsNull(!Patient_ID) Then
           varOtherNames = !OtherNames
        End If
        If Not IsNull(!Patient_ID) Then
           varSurName = !SurName
        End If
        If Not IsNull(!Patient_ID) Then
           varTitle_ID = !Title_ID
        End If
        If Not IsNull(!Patient_ID) Then
           varSex_ID = !Sex_ID
        End If
        If Not IsNull(!Patient_ID) Then
           varRace_ID = !Race_ID
        End If
        If Not IsNull(!Patient_ID) Then
           varMarital_ID = !Marital_ID
        End If
        If Not IsNull(!Patient_ID) Then
           varNICNo = !NICNo
        End If
        If Not IsNull(!Patient_ID) Then
           varBirthCertificateNo = !BirthCertificateNo
        End If
        If Not IsNull(!Patient_ID) Then
           varAddress = !Address
        End If
        If Not IsNull(!Patient_ID) Then
           varPhone = !Phone
        End If
        If Not IsNull(!Patient_ID) Then
           varFax = !Fax
        End If
        If Not IsNull(!Patient_ID) Then
           varEmail = !Email
        End If
        If Not IsNull(!Patient_ID) Then
           varDateOfBirth = !DateOfBirth
        End If
        If Not IsNull(!Patient_ID) Then
           varNotes = !Notes
        End If
        If Not IsNull(!Patient_ID) Then
           varPhoto = !Photo
        End If
        If Not IsNull(!Patient_ID) Then
           varCredit = !Credit
        End If
        If Not IsNull(!Patient_ID) Then
           varMaxCredit = !MaxCredit
        End If
        If Not IsNull(!Patient_ID) Then
           varBlackListedPatient = !BlackListedPatient
        End If
        If Not IsNull(!Patient_ID) Then
           varRegisteredDate = !RegisteredDate
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varPatient_ID = 0
    varPatient_ID2 = Empty
    varFirstName = Empty
    varOtherNames = Empty
    varSurName = Empty
    varTitle_ID = 0
    varSex_ID = 0
    varRace_ID = 0
    varMarital_ID = 0
    varNICNo = Empty
    varBirthCertificateNo = Empty
    varAddress = Empty
    varPhone = Empty
    varFax = Empty
    varEmail = Empty
    varDateOfBirth = Empty
    varNotes = Empty
    varPhoto = Empty
    varCredit = 0
    varMaxCredit = 0
    varBlackListedPatient = False
    varRegisteredDate = Empty
End Sub

Public Property Let Patient_ID(ByVal vPatient_ID As Long)
    Call clearData
    varPatient_ID = vPatient_ID
    Call loadData
End Property

Public Property Get Patient_ID() As Long
    Patient_ID = varPatient_ID
End Property

Public Property Let Patient_ID2(ByVal vPatient_ID2 As String)
    varPatient_ID2 = vPatient_ID2
End Property

Public Property Get Patient_ID2() As String
    Patient_ID2 = varPatient_ID2
End Property

Public Property Let FirstName(ByVal vFirstName As String)
    varFirstName = vFirstName
End Property

Public Property Get FirstName() As String
    FirstName = varFirstName
End Property

Public Property Let OtherNames(ByVal vOtherNames As String)
    varOtherNames = vOtherNames
End Property

Public Property Get OtherNames() As String
    OtherNames = varOtherNames
End Property

Public Property Let SurName(ByVal vSurName As String)
    varSurName = vSurName
End Property

Public Property Get SurName() As String
    SurName = varSurName
End Property

Public Property Let Title_ID(ByVal vTitle_ID As Long)
    varTitle_ID = vTitle_ID
End Property

Public Property Get Title_ID() As Long
    Title_ID = varTitle_ID
End Property

Public Property Let Sex_ID(ByVal vSex_ID As Long)
    varSex_ID = vSex_ID
End Property

Public Property Get Sex_ID() As Long
    Sex_ID = varSex_ID
End Property

Public Property Let Race_ID(ByVal vRace_ID As Long)
    varRace_ID = vRace_ID
End Property

Public Property Get Race_ID() As Long
    Race_ID = varRace_ID
End Property

Public Property Let Marital_ID(ByVal vMarital_ID As Long)
    varMarital_ID = vMarital_ID
End Property

Public Property Get Marital_ID() As Long
    Marital_ID = varMarital_ID
End Property

Public Property Let NICNo(ByVal vNICNo As String)
    varNICNo = vNICNo
End Property

Public Property Get NICNo() As String
    NICNo = varNICNo
End Property

Public Property Let BirthCertificateNo(ByVal vBirthCertificateNo As String)
    varBirthCertificateNo = vBirthCertificateNo
End Property

Public Property Get BirthCertificateNo() As String
    BirthCertificateNo = varBirthCertificateNo
End Property

Public Property Let Address(ByVal vAddress As String)
    varAddress = vAddress
End Property

Public Property Get Address() As String
    Address = varAddress
End Property

Public Property Let Phone(ByVal vPhone As String)
    varPhone = vPhone
End Property

Public Property Get Phone() As String
    Phone = varPhone
End Property

Public Property Let Fax(ByVal vFax As String)
    varFax = vFax
End Property

Public Property Get Fax() As String
    Fax = varFax
End Property

Public Property Let Email(ByVal vEmail As String)
    varEmail = vEmail
End Property

Public Property Get Email() As String
    Email = varEmail
End Property

Public Property Let DateOfBirth(ByVal vDateOfBirth As Date)
    varDateOfBirth = vDateOfBirth
End Property

Public Property Get DateOfBirth() As Date
    DateOfBirth = varDateOfBirth
End Property

Public Property Let Notes(ByVal vNotes As String)
    varNotes = vNotes
End Property

Public Property Get Notes() As String
    Notes = varNotes
End Property

Public Property Let Photo(ByVal vPhoto As String)
    varPhoto = vPhoto
End Property

Public Property Get Photo() As String
    Photo = varPhoto
End Property

Public Property Let Credit(ByVal vCredit As Double)
    varCredit = vCredit
End Property

Public Property Get Credit() As Double
    Credit = varCredit
End Property

Public Property Let MaxCredit(ByVal vMaxCredit As Double)
    varMaxCredit = vMaxCredit
End Property

Public Property Get MaxCredit() As Double
    MaxCredit = varMaxCredit
End Property

Public Property Let BlackListedPatient(ByVal vBlackListedPatient As Boolean)
    varBlackListedPatient = vBlackListedPatient
End Property

Public Property Get BlackListedPatient() As Boolean
    BlackListedPatient = varBlackListedPatient
End Property

Public Property Let RegisteredDate(ByVal vRegisteredDate As Date)
    varRegisteredDate = vRegisteredDate
End Property

Public Property Get RegisteredDate() As Date
    RegisteredDate = varRegisteredDate
End Property


