VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Title"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Title"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Dim temSQL As String
Private varTitle_ID As Long
Private varTitle As String

Private Sub clearData()
varTitle_ID = 0
varTitle = Empty
End Sub

Public Property Let Title_ID(ByVal vTitle_ID As Long)
Call clearData
    varTitle_ID = vTitle_ID
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblTitle WHERE Title_ID = " & varTitle_ID
        .Open temSQL, cnnChanneling, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
    If Not IsNull(!Title_ID) Then
            varTitle_ID = !Title_ID
    End If
    If Not IsNull(!Title) Then
            varTitle = !Title
    End If
        End If
 If .State = 1 Then .Close
    End With
    
End Property

Public Property Get Title_ID() As Long
    Title_ID = varTitle_ID
End Property

Public Property Let Title(ByVal vTitle As String)
    varTitle = vTitle
End Property

Public Property Get Title() As String
    Title = varTitle
End Property


