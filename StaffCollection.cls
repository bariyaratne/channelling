VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StaffCollection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"staff"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarstaff As staff 'local copy
Private mvarstaffFee As Double 'local copy
Private mvarhospitalFee As Double 'local copy
Private mvarotherFee As Double 'local copy
Private mvartotalFee As Double 'local copy
Private mvarcashDate As Date 'local copy
Private mvarappDate As Date 'local copy
Public Property Let appDate(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.appDate = 5
    mvarappDate = vData
End Property


Public Property Get appDate() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.appDate
    appDate = mvarappDate
End Property



Public Property Let cashDate(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.cashDate = 5
    mvarcashDate = vData
End Property


Public Property Get cashDate() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.cashDate
    cashDate = mvarcashDate
End Property



Public Property Let totalFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.totalFee = 5
    mvartotalFee = vData
End Property


Public Property Get totalFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.totalFee
    totalFee = mvartotalFee
End Property



Public Property Let otherFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.otherFee = 5
    mvarotherFee = vData
End Property


Public Property Get otherFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.otherFee
    otherFee = mvarotherFee
End Property



Public Property Let hospitalFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.hospitalFee = 5
    mvarhospitalFee = vData
End Property


Public Property Get hospitalFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.hospitalFee
    hospitalFee = mvarhospitalFee
End Property



Public Property Let staffFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.staffFee = 5
    mvarstaffFee = vData
End Property


Public Property Get staffFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.staffFee
    staffFee = mvarstaffFee
End Property



Public Property Set staff(ByVal vData As staff)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.staff = Form1
    Set mvarstaff = vData
End Property


Public Property Get staff() As staff
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.staff
    Set staff = mvarstaff
End Property



